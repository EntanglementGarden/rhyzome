// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package common

const NetworkIDBootstrap = "entanglement-bootstrap-network"

// ProvisioningEndpoints defines model for ProvisioningEndpoints.
type ProvisioningEndpoints struct {
	LibvirtGRPC string `json:"libvirt_grpc"`
	OpenWRTGRPC string `json:"openwrt_grpc"`
	Step        string `json:"step"`
}

// ProvisioningInfo defines model for ProvisioningInfo.
type ProvisioningInfo struct {
	CAFingerprint string                `json:"ca_fingerprint"`
	Domain        string                `json:"domain"`
	Endpoints     ProvisioningEndpoints `json:"endpoints"`
}
