// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package common

import (
	"crypto/rand"
	"fmt"
)

// GenerateMAC generates a mac address, based on https://stackoverflow.com/questions/21018729/generate-mac-address-in-go
func GenerateMAC(prefix string) (string, error) {
	buf := make([]byte, 3)
	_, err := rand.Read(buf)
	if err != nil {
		return "", fmt.Errorf("error generating mac: %v", err)
	}

	// set local bit and unicast
	buf[0] = (buf[0] | 2) & 0xfe
	// Set the local bit
	buf[0] |= 2

	// avoid libvirt-reserved addresses
	if buf[0] == 0xfe {
		buf[0] = 0xee
	}

	return fmt.Sprintf("%s:%02x:%02x:%02x", prefix, buf[0], buf[1], buf[2]), nil
}
