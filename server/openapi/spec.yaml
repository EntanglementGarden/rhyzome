openapi: "3.0.0"
info:
  version: 0.1.0
  title: Rhyzome Instances v0 API
  description: everything subject to change without notice
  contact:
    name: Entanglement Garden
    url: https://entanglement.garden
    email: info@entanglement.garden
  license:
    name: CC-BY-SA-4.0
    url: https://creativecommons.org/licenses/by/4.0/
security:
  - EntanglementAuth: []
servers:
  - url: /entanglement.garden/rhyzome
paths:
  /v0/instance:
    get:
      description: List all instances
      operationId: list_all_instances
      security:
        - EntanglementAuth: ["entanglement.garden:rhyzome:instance:*#list"]
      responses:
        '200':
          description: a list of all instances
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Instance'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: ../../../common/openapi/spec.yaml#/components/schemas/Error
    post:
      description: Create an instance
      operationId: create_instance
      requestBody:
        description: the new instance to create
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateInstanceRequestBody'
      security:
        - EntanglementAuth: ["entanglement.garden:rhyzome:instance:*#create"]
      responses:
        '201':
          description: the instance is being created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Instance'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: ../../../common/openapi/spec.yaml#/components/schemas/Error
  /v0/instance/{id}:
    get:
      description: details about an instance
      operationId: get_instance
      parameters:
        - name: id
          in: path
          description: ID of instance to fetch
          required: true
          schema:
            type: string
      security:
        - EntanglementAuth: ["entanglement.garden:rhyzome:instance:{id}#read"]
      responses:
        '200':
          description: the instance
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Instance'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: ../../../common/openapi/spec.yaml#/components/schemas/Error
    delete:
      description: delete an instance
      operationId: delete_instance
      parameters:
        - name: id
          in: path
          description: ID of instance to delete
          required: true
          schema:
            type: string
      security:
        - EntanglementAuth: ["entanglement.garden:rhyzome:instance:{id}#delete"]
      responses:
        '204':
          description: instance is marked for deletion
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: ../../../common/openapi/spec.yaml#/components/schemas/Error
  /v0/instance/{id}/start:
    post:
      description: start an instance
      operationId: start_instance
      parameters:
        - name: id
          in: path
          description: ID of instance to start
          required: true
          schema:
            type: string
      security:
        - EntanglementAuth: ["entanglement.garden:rhyzome:instance:{id}#start"]
      responses:
        '204':
          description: instance is being started
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: ../../../common/openapi/spec.yaml#/components/schemas/Error
  /v0/instance/{id}/stop:
    post:
      description: stop an instance
      operationId: stop_instance
      parameters:
        - name: id
          in: path
          description: ID of instance to stop
          required: true
          schema:
            type: string
      security:
        - EntanglementAuth: ["entanglement.garden:rhyzome:instance:{id}#stop"]
      responses:
        '204':
          description: instance is being stopped
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: ../../../common/openapi/spec.yaml#/components/schemas/Error
  /v0/instance/{id}/reboot:
    post:
      description: reboot an instance
      operationId: reboot_instance
      parameters:
        - name: id
          in: path
          description: ID of instance to reboot
          required: true
          schema:
            type: string
      security:
        - EntanglementAuth: ["entanglement.garden:rhyzome:instance:{id}#reboot"]
      responses:
        '204':
          description: instance is being rebooted
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: ../../../common/openapi/spec.yaml#/components/schemas/Error
  /v0/instance/{instance}/nic:
    post:
      description: attach this instance to a network by creating a nic
      operationId: attach_nic
      parameters:
        - name: instance
          in: path
          description: ID of the instance to attach a nic to
          required: true
          schema:
            type: string
      security:
        - EntanglementAuth: ["entanglement.garden:rhyzome:instance:{instance}#attach-nic"]
      requestBody:
        description: the network ID to attach to
        required: true
        content:
          application/json:
            schema:
              type: string
      responses:
        '201':
          description: the nic has been created and the instance is attached to the network
          content:
            application/json:
              $ref: '#/components/schemas/NetworkInterface'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: ../../../common/openapi/spec.yaml#/components/schemas/Error
  /v0/instance/{id}/nic/{nic}/add-trunk-vlan:
    post:
      description: add a vlan to a trunk port nic
      operationId: trunk_nic_add_network
      parameters:
        - name: id
          in: path
          description: the ID of the instance the NIC is attached to
          required: true
          schema:
            type: string
        - name: nic
          in: path
          description: the ID of the NIC to add the network to
          required: true
          schema:
            type: string
      security:
        - EntanglementAuth: ["entanglement.garden:rhyzome:instance-nic:*#add-trunk-vlan"]
      requestBody:
        description: the name of the new nic
        required: true
        content:
          application/json:
            schema:
              type: string
      responses:
        '204':
          description: the trunk nic was successfully updated
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: ../../../common/openapi/spec.yaml#/components/schemas/Error
  /v0/instance/{id}/nic/{nic}:
    delete:
      description: remove a NIC from an instance
      operationId: remove_nic
      parameters:
        - name: id
          in: path
          description: the ID of the instance the NIC is attached to
          required: true
          schema:
            type: string
        - name: nic
          in: path
          description: the ID of the NIC
          required: true
          schema:
            type: string
      security:
        - EntanglementAuth: ["entanglement.garden:rhyzome:instance:{id}#remove-nic"]
      responses:
        '204':
          description: the nic was successfully deleted
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: ../../../common/openapi/spec.yaml#/components/schemas/Error
  /v0/bootstrap-system-network-ready:
    post:
      description: indicate that the system network is ready
      operationId: bootstrap_system_network_ready
      requestBody:
        description: the IP assigned to the grpc host on the system network
        required: true
        content:
          application/json:
            schema:
              type: string
      security:
        - EntanglementAuth: ["entanglement.garden:rhyzome:system:*#bootstrap"]
      responses:
        '204': {}
        '404':
          description: already completed
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: ../../../common/openapi/spec.yaml#/components/schemas/Error
  /v0/bootstrap-complete:
    post:
      description: indicate that bootstrapping is completed
      operationId: bootstrap_complete
      security:
        - EntanglementAuth: ["entanglement.garden:rhyzome:system:*#bootstrap"]
      responses:
        '204':
          description: bootstrap networks removed
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: ../../../common/openapi/spec.yaml#/components/schemas/Error
components:
  securitySchemes:
    EntanglementAuth:
      type: http
      scheme: bearer
  schemas:
    Instance:
      type: object
      required:
        - resource_id  
        - memory
        - cpu
        - root_volume
        - state
        - interfaces
      properties:
        resource_id:
          type: string
        memory:
          type: integer
        cpu:
          type: integer
        node:
          type: string
        user_data:
          type: string
        root_volume:
          type: string
        state:
          type: string
          enum:
            - "PENDING"
            - "CREATING"
            - "STARTING"
            - "RUNNING"
            - "STOPPING"
            - "STOPPED"
            - "DELETING"
            - "DELETED"
            - "ERROR"
        interfaces:
          type: array
          items:
            $ref: '#/components/schemas/NetworkInterface'
    CreateInstanceRequestBody:
      type: object
      required:
        - memory
        - cpu
        - base_image
        - base_image_qcow2
        - disk_size
        - networks
      properties:
        memory:
          description: memory (in MB) to allocate to the new instance
          type: integer
        cpu:
          description: number of CPU cores to allocate to the new instance
          type: integer
        user_data:
          description: arbitrary data that will be available to the VM
          type: string
        base_image_qcow2:
          description: URL will be downloaded and written to the root disk if the system uses qcow2 storage
          type: string
        base_image:
          description: URL will be downloaded and written to the root disk if the system does not use qcow2 storage
          type: string
        disk_size:
          description: the size of the root disk, in gigabytes
          type: integer
        networks:
          description: a list of networks to connect the new instance to. a NIC will be added for each network.
          type: array
          items:
            type: string
        special_cases:
          description: a list of special cases to apply to the new instance
          type: array
          items:
            type: string
    NetworkInterface:
      type: object
      required:
        - id
        - addresses
        - mac
        - network
      properties:
        id:
          type: string
        addresses:
          type: array
          items:
            type: string
        mac:
          type: string
        network:
          type: string
        trunk:
          description: indicates this port NIC will receive vlan-tagged traffic
          type: boolean
