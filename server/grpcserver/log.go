// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver

import (
	"net"

	"github.com/sirupsen/logrus"
)

type loggingListener struct {
	next net.Listener
}

func (l loggingListener) Accept() (net.Conn, error) {
	conn, err := l.next.Accept()
	if err != nil {
		return conn, err
	}

	logrus.Debug("accepted connection from ", conn.RemoteAddr())

	return conn, err
}

func (l loggingListener) Close() error {
	return l.next.Close()
}

func (l loggingListener) Addr() net.Addr {
	return l.next.Addr()
}
