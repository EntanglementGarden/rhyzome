// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver

import (
	"context"
	"fmt"
	"net"

	"google.golang.org/grpc"

	"entanglement.garden/common/grpcx"
	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/protos"
	"entanglement.garden/rhyzome/server/config"
)

type server struct {
	protos.UnimplementedRhyzomeServer
}

var grpcServer *grpc.Server

func Listen() {
	grpcServer = grpcx.NewServer(config.C.PKI)
	log := logging.GetLog(context.Background())

	bind := fmt.Sprintf(":%d", config.C.GRPCPort)
	listener, err := net.Listen("tcp", bind)
	if err != nil {
		log.Error("grpc server failed to listen:", err)
		return
	}

	protos.RegisterRhyzomeServer(grpcServer, server{})

	log.WithField("bind", bind).Info("Starting gRPC server")
	err = grpcServer.Serve(loggingListener{next: listener})
	if err != nil {
		log.Fatal("error from grpc listener: ", err)
	}
}

func Shutdown() {
	grpcServer.Stop()
}
