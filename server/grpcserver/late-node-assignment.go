// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver

import (
	"context"
	"database/sql"
	"fmt"
	"math/rand"

	"entanglement.garden/common/dbx"
	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/protos"
	"entanglement.garden/rhyzome/server/db"
)

// lastNodeAssignment checks for any instances that aren't assigned a node, and if it finds any
// assigns them to one of the currently connected nodes. Designed with some protection against problems
// caused by concurrent execution, but concurrency has not been tested at this time.
//
// currently called once every time a new node connects
func lateNodeAssignment(dbConn *sql.DB, q *db.Queries) error {
	for {
		ctx := context.Background()

		tx, err := dbConn.Begin()
		if err != nil {
			return err
		}
		defer dbx.RollbackTx(ctx, tx)

		queries := q.WithTx(tx)

		instance, err := queries.GetUnassignedInstance(ctx)
		if err != nil {
			if err == sql.ErrNoRows { // no unassigned instances
				return nil
			}

			return err
		}

		ctx, log := logging.LogWithField(ctx, "instance", instance.ID)

		log.Debug("trying to assign instance to node")

		nodes, err := queries.GetAvailableNodes(ctx)
		if err != nil {
			return fmt.Errorf("error getting available nodes for unassigned instance: %v", err)
		}
		node := nodes[rand.Intn(len(nodes))]

		volume, err := queries.GetVolumeByID(ctx, instance.RootVolume)
		if err != nil {
			return fmt.Errorf("error finding instance's root node in database: %v", err)
		}

		err = queries.AssignInstanceToNode(ctx, db.AssignInstanceToNodeParams{
			Node: db.NullString(node),
			ID:   instance.ID,
		})
		if err != nil {
			return fmt.Errorf("error assigning instance to node: %v", err)
		}

		err = queries.AssignVolumeToNode(ctx, db.AssignVolumeToNodeParams{
			Node: db.NullString(node),
			ID:   volume.ID,
		})
		if err != nil {
			return fmt.Errorf("error assigning volume to node: %v", err)
		}

		grpcInstance, err := instance.GetGRPC(ctx, queries)
		if err != nil {
			return fmt.Errorf("error converting instance to gRPC: %v", err)
		}

		if err := tx.Commit(); err != nil {
			log.Warn("failed to commit transaction assigning instance to node")
			return err
		}

		PushEvents(ctx, node,
			&protos.Event{
				Volume:   volume.GetGRPC(),
				Instance: grpcInstance,
			})
	}
}
