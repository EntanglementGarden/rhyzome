package grpcserver

import (
	"context"

	"github.com/sirupsen/logrus"

	"entanglement.garden/rhyzome/protos"
	"entanglement.garden/rhyzome/server/db"
)

func (server) InstanceStateChange(ctx context.Context, change *protos.Instance) (*protos.Empty, error) {
	queries, dbConn, err := db.Get()
	if err != nil {
		return nil, err
	}
	defer dbConn.Close()

	err = queries.SetInstanceState(ctx, db.SetInstanceStateParams{
		ID:    change.Id,
		State: int64(change.State),
	})
	if err != nil {
		return nil, err
	}

	logrus.Debug("instance ", change.Id, " state changed to ", change.State.String())

	return &protos.Empty{}, nil
}

func (server) VolumeStateChange(ctx context.Context, change *protos.Volume) (*protos.Empty, error) {
	queries, dbConn, err := db.Get()
	if err != nil {
		return nil, err
	}
	defer dbConn.Close()

	err = queries.SetVolumeState(ctx, db.SetVolumeStateParams{
		ID:    change.Id,
		State: int64(change.State),
	})
	if err != nil {
		return nil, err
	}

	logrus.Debug("volume ", change.Id, " state changed to ", change.State.String())

	return &protos.Empty{}, nil
}
