// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"sync"
	"time"

	"entanglement.garden/common/grpcx"
	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/protos"
	"entanglement.garden/rhyzome/server/db"
	"github.com/sirupsen/logrus"
)

var (
	eventsChannels      = make(map[string]chan *protos.Event)
	eventsChannelsMutex = sync.Mutex{}
)

func (server) EventStream(events protos.Rhyzome_EventStreamServer) error {
	ctx := events.Context()

	peer, err := grpcx.GetPeerName(ctx)
	if err != nil {
		return err
	}

	nodeName := peer.CommonName

	ctx, log := logging.LogWithField(ctx, "node", nodeName)
	log.Info("grpc connection from ", peer)

	queries, dbConn, err := db.Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	node, err := getOrAddNode(ctx, queries, nodeName)
	if err != nil {
		log.Error("error getting node from db: ", err)
		return err
	}

	ch, err := addEventChannel(ctx, node)
	if err != nil {
		return err
	}
	defer removeEventChannel(ctx, node)

	if err := ensureExistingResourcesExist(ctx, queries, node, events); err != nil {
		return err
	}

	go func() {
		for ctx.Err() == nil {
			if err := lateNodeAssignment(dbConn, queries); err != nil {
				logrus.Error("error assigning unassigned instances: ", err)
				time.Sleep(time.Second * 5)
				continue
			} else {
				break
			}
		}
	}()

	for {
		select {
		case e := <-ch:
			err := events.Send(e)
			if err != nil {
				return err
			}
		case <-ctx.Done():
			return nil
		}
	}
}

func getOrAddNode(ctx context.Context, queries *db.Queries, hostname string) (db.Node, error) {
	node, err := queries.GetNodeByHostname(ctx, hostname)
	if err == sql.ErrNoRows {
		node, err = addNode(ctx, queries, hostname)
	}
	if err != nil {
		return db.Node{}, err
	}
	return node, nil
}

func addNode(ctx context.Context, queries *db.Queries, hostname string) (db.Node, error) {
	ctx, log := logging.LogWithFields(ctx, map[string]any{"node_id": hostname, "hostname": hostname})
	log.Info("registering new node")

	node, err := queries.AddNode(ctx, db.AddNodeParams{
		ID:        hostname,
		Hostname:  hostname,
		Available: true,
	})
	if err != nil {
		return db.Node{}, err
	}

	return node, nil
}

func addEventChannel(ctx context.Context, node db.Node) (chan *protos.Event, error) {
	eventsChannelsMutex.Lock()
	_, ok := eventsChannels[node.ID]
	if ok {
		logging.GetLog(ctx).Error("refusing duplicate connection from ", node.ID)
		eventsChannelsMutex.Unlock()
		return nil, errors.New("duplicate connection from node not allowed")
	}

	ch := make(chan *protos.Event, 100)
	eventsChannels[node.ID] = ch
	eventsChannelsMutex.Unlock()

	return ch, nil
}

func removeEventChannel(ctx context.Context, node db.Node) {
	log := logging.GetLog(ctx)
	log.Debug("lost connection from ", node.ID)
	eventsChannelsMutex.Lock()
	ch, ok := eventsChannels[node.ID]
	if !ok {
		log.Debug("EventStream endpoint closed with no clients for this node")
		eventsChannelsMutex.Unlock()
		return
	}
	close(ch)
	delete(eventsChannels, node.ID)
	eventsChannelsMutex.Unlock()
}

func ensureExistingResourcesExist(ctx context.Context, queries *db.Queries, node db.Node, events protos.Rhyzome_EventStreamServer) error {
	log := logging.GetLog(ctx)

	volumes, err := queries.GetVolumesForNode(ctx, sql.NullString{String: node.ID, Valid: true})
	if err != nil {
		return err
	}

	log.Debug("ensuring state of ", len(volumes), " volumes")

	for _, volume := range volumes {
		err = events.Send(&protos.Event{Volume: volume.GetGRPC()})
		if err != nil {
			return err
		}
	}

	instances, err := queries.GetInstancesForNode(ctx, sql.NullString{String: node.ID, Valid: true})
	if err != nil {
		return err
	}

	log.Debug("ensuring state of ", len(instances), " instances")

	for _, instance := range instances {
		log.Debug("ensuring instance ", instance.ID)
		instance, err := instance.GetGRPC(ctx, queries)
		if err != nil {
			return fmt.Errorf("error converting instance in database to gRPC: %v", err)
		}

		err = events.Send(&protos.Event{Instance: instance})
		if err != nil {
			log.Warn("error ensuring instances on (re)connecting node")
			return err
		}
	}

	return nil
}

// PushEvents to a connected node. if relevant node is not connected, messages is silently ignored
func PushEvents(ctx context.Context, node string, events ...*protos.Event) {
	log := logging.GetLog(ctx)

	eventsChannelsMutex.Lock()
	defer eventsChannelsMutex.Unlock()

	ch, ok := eventsChannels[node]
	if !ok {
		return
	}

	for _, event := range events {
		log.Debug("pushing event to node ", node, ":", event)
		ch <- event
	}
}
