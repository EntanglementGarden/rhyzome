// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/sirupsen/logrus"

	"entanglement.garden/common/grpcx"
	"entanglement.garden/common/identifiers"
	"entanglement.garden/rhyzome/protos"
	"entanglement.garden/rhyzome/server/db"
)

const importSucksWarning = "the VM import feature is currently extremely limited and only supports NICs on the system or bootstrap networks"

var (
	knownNetworks = map[int32]string{
		10: "system",
	}
)

func (server) Import(ctx context.Context, event *protos.Event) (*protos.Empty, error) {
	peer, err := grpcx.GetPeerName(ctx)
	if err != nil {
		return nil, err
	}

	logrus.Debug("importing ", event)

	queries, dbConn, err := db.Get()
	if err != nil {
		return nil, err
	}
	defer dbConn.Close()

	if event.Volume != nil {
		_, err = queries.GetVolumeByID(ctx, event.Volume.Id)
		if err == sql.ErrNoRows {
			err = queries.CreateVolume(ctx, db.CreateVolumeParams{
				ID:          event.Volume.Id,
				Tenant:      identifiers.DefaultTenant,
				SourceImage: db.NullString(event.Volume.SourceImage),
			})
			if err != nil {
				logrus.Error("error creating volume: ", err)
				return nil, fmt.Errorf("error creating volume: %v", err)
			}
		}
	}

	if event.Instance != nil {
		_, err = queries.GetInstance(ctx, event.Instance.Id)
		if err == sql.ErrNoRows {
			_, err = queries.CreateInstance(ctx, db.CreateInstanceParams{
				ID:         event.Instance.Id,
				Tenant:     identifiers.DefaultTenant,
				Name:       event.Instance.Id,
				Memory:     int64(event.Instance.Memory),
				Cpu:        int64(event.Instance.Cores),
				RootVolume: event.Instance.RootVolume,
				BiosData:   db.NullString(event.Instance.Biosdata),
			})
			if err != nil {
				logrus.Error("error creating instance: ", err)
				return nil, fmt.Errorf("error creating instance: %v", err)
			}

			err = queries.AssignInstanceToNode(ctx, db.AssignInstanceToNodeParams{
				Node: db.NullString(peer.CommonName),
				ID:   event.Instance.Id,
			})
			if err != nil {
				logrus.Error("error assigning instance to node: ", err)
				return nil, fmt.Errorf("error assigning instance to node: %v", err)
			}
		}

		for _, nic := range event.Instance.NetworkInterfaces {
			network := ""

			switch {
			case nic.Bootstrap:
				network = db.NetworkIDBootstrap
			case len(nic.Vlan) == 1:
				var ok bool
				network, ok = knownNetworks[nic.Vlan[0]]
				if !ok {
					return nil, fmt.Errorf("unknown vlan %d - %s", nic.Vlan[0], importSucksWarning)
				}
			default:
				return nil, fmt.Errorf("unsupported NIC %+v - %s", nic, importSucksWarning)
			}

			err = queries.CreateNIC(ctx, db.CreateNICParams{
				ID:       nic.Id,
				Instance: event.Instance.Id,
				Mac:      nic.Mac,
				Ip:       nic.Ip,
				Network:  network,
				Trunk:    nic.Tagged,
			})
			if err != nil {
				logrus.Error("error adding nic: ", err)
				return nil, fmt.Errorf("error adding nic: %v", err)
			}
		}
	}

	return &protos.Empty{}, nil
}
