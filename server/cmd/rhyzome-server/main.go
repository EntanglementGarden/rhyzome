// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"

	"entanglement.garden/rhyzome/server/config"
	"entanglement.garden/rhyzome/server/db"
	"entanglement.garden/rhyzome/server/grpcserver"
	"entanglement.garden/rhyzome/server/httpserver"
)

var signals = make(chan os.Signal, 1)

func main() {
	signal.Notify(signals, syscall.SIGINT, syscall.SIGHUP)

	config.Load()
	logrus.SetLevel(logrus.TraceLevel)

	if err := db.Migrate(); err != nil {
		logrus.Fatal("error running migrations: ", err)
	}

	go grpcserver.Listen()
	go httpserver.Serve()

	for {
		switch <-signals {
		case syscall.SIGHUP:
			config.Load()
		case syscall.SIGINT:
			grpcserver.Shutdown()
			httpserver.Shutdown()
			return
		}
	}
}
