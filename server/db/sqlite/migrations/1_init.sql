-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- +goose Up
-- +goose StatementBegin
CREATE TABLE nodes (
    id TEXT UNIQUE NOT NULL,
    hostname TEXT NOT NULL,
    available BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE instances (
    id TEXT UNIQUE NOT NULL,
    tenant TEXT NOT NULL,
    name TEXT NOT NULL,
    memory INTEGER NOT NULL,
    cpu INTEGER NOT NULL,
    node TEXT REFERENCES nodes(id),
    state INTEGER NOT NULL DEFAULT 0,
    user_data BLOB,
    root_volume TEXT NOT NULL REFERENCES volumes(id),
    bios_data TEXT
);

CREATE TABLE volumes (
    id TEXT UNIQUE NOT NULL,
    tenant TEXT NOT NULL,
    node TEXT REFERENCES nodes(id),
    size INTEGER NOT NULL,
    state INTEGER NOT NULL DEFAULT 0,
    source_image TEXT,
    source_image_qcow2 TEXT,
    attached_instance TEXT REFERENCES instances(id)
);

CREATE TABLE networks (
    id TEXT UNIQUE NOT NULL,
    vlan INTEGER UNIQUE NOT NULL
);

CREATE TABLE instance_nics (
    id TEXT UNIQUE NOT NULL,
    instance TEXT REFERENCES instances(resource_id) NOT NULL,
    mac TEXT UNIQUE NOT NULL,
    ip TEXT NOT NULL,
    network TEXT NOT NULL REFERENCES networks(id),
    trunk BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE trunk_networks (
    nic TEXT NOT NULL REFERENCES instance_nics(id),
    network TEXT NOT NULL REFERENCES networks(id),

    CONSTRAINT unique_vlan UNIQUE(nic, network)
);

CREATE TABLE special_cases (
    instance TEXT NOT NULL REFERENCES instances(id),
    special_case TEXT NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE instances;
DROP TABLE volumes;
DROP TABLE nodes;
-- +goose StatementEnd
