-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- name: CreateInstance :one
INSERT INTO instances (id, tenant, name, memory, cpu, user_data, root_volume, bios_data) VALUES (?, ?, ?, ?, ?, ?, ?, ?) RETURNING *;

-- name: AssignInstanceToNode :exec
UPDATE instances SET node = ? WHERE id = ?;

-- name: SetInstanceState :exec
UPDATE instances SET state = ? WHERE id = ?;

-- name: GetInstance :one
SELECT * FROM instances WHERE id = ?;

-- name: ListInstances :many
SELECT * FROM instances WHERE tenant = ?;

-- name: GetUnassignedInstance :one
SELECT * FROM instances WHERE node IS NULL LIMIT 1;

-- name: AttachVolumeToInstance :execrows
UPDATE volumes SET attached_instance = ? WHERE id = ?;

-- name: DetachVolume :execrows
UPDATE volumes SET instance = NULL WHERE id = ?;

-- name: DeleteInstance :exec
DELETE FROM instances WHERE id = ?;

-- name: GetInstancesForNode :many
SELECT * FROM instances WHERE node = ?;

-- name: GetSpecialCases :many
SELECT special_case FROM special_cases WHERE instance = ?;

-- name: SetSpecialCase :exec
INSERT INTO special_cases (instance, special_case) VALUES (?, ?);

-- name: DeleteSpecialCases :exec
DELETE FROM special_cases WHERE instance = ?;
