-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- name: CreateVolume :exec
INSERT INTO volumes (id, tenant, source_image, source_image_qcow2, size) VALUES (?, ?, ?, ?, ?);

-- name: ListVolumes :many
SELECT * FROM volumes WHERE tenant = ?;

-- name: SetVolumeState :exec
UPDATE volumes SET state = ? WHERE id = ?;

-- name: GetVolumesForNode :many
SELECT * FROM volumes WHERE node = ?;

-- name: GetImageForVolume :one
SELECT source_image FROM volumes WHERE id = ?;

-- name: AssignVolumeToNode :exec
UPDATE volumes SET node = ? WHERE id = ?;

-- name: GetInstanceAttachedVolumes :many
SELECT * FROM volumes WHERE attached_instance = ?;

-- name: GetVolumeByID :one
SELECT * FROM volumes WHERE id = ?;

-- name: DeleteVolume :exec
DELETE FROM volumes WHERE id = ?;
