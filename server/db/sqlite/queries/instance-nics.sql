-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- name: CreateNIC :exec
INSERT INTO instance_nics (id, instance, mac, ip, network, trunk) VALUES (?, ?, ?, ?, ?, ?);

-- name: DeleteNIC :exec
DELETE FROM instance_nics WHERE instance = ? AND id = ?;

-- name: GetNICsByInstance :many
SELECT * FROM instance_nics WHERE instance = ?;

-- name: DeleteNICsByInstance :exec
DELETE FROM instance_nics WHERE instance = ?;

-- name: GetTrunkVlans :many
SELECT networks.vlan FROM trunk_networks, networks WHERE trunk_networks.nic = ? AND networks.id = trunk_networks.network;

-- name: AddTrunkNetwork :exec
INSERT INTO trunk_networks (nic, network) VALUES (?, ?);

-- name: RemoveTrunkNetwork :exec
DELETE FROM trunk_networks WHERE nic = ? AND network = ?;

-- name: GetVlanForNetwork :one
SELECT vlan FROM networks WHERE id = ?;

-- name: GetNetworkForVlan :one
SELECT id FROM networks WHERE vlan = ?;

-- name: AddNetwork :exec
INSERT INTO networks (id, vlan) VALUES (?, ?);

-- name: GetNetworkByID :one
SELECT * FROM networks WHERE id = ?;

-- name: RemoveAllNicsOnNetwork :many
DELETE FROM instance_nics WHERE network = ? RETURNING instance;

-- name: SetNICIP :exec
UPDATE instance_nics SET ip = ? WHERE id = ?;

-- name: GetNICIP :one
SELECT ip FROM instance_nics WHERE id = ?;
