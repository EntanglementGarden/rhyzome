-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- name: GetNodes :many
SELECT * FROM nodes;

-- name: GetAvailableNodes :many
SELECT id FROM nodes WHERE available = true;

-- name: GetNodeByHostname :one
SELECT * FROM nodes WHERE hostname = ?;

-- name: AddNode :one
INSERT INTO nodes (id, hostname, available) VALUES (?, ?, ?) RETURNING *;

-- name: SetNodeHostname :execrows
UPDATE nodes SET hostname = ? WHERE id = ?;

-- name: SetNodeAvailability :execrows
UPDATE nodes SET available = ? WHERE hostname = ?;

-- name: DeleteNode :execrows
DELETE FROM nodes WHERE id = ?;

-- name: GetNodeForInstance :one
SELECT nodes.* FROM nodes, instances WHERE nodes.id = instances.node AND instances.id = ?;

-- name: SetNodeForInstance :execrows
UPDATE instances SET node = ? WHERE id = ?;
