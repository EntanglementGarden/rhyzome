// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import (
	"database/sql"
	"embed"

	goose "github.com/pressly/goose/v3"
)

const (
	NetworkIDWAN       = "wan"
	NetworkIDBootstrap = "entanglement-bootstrap-network"
)

//go:embed sqlite/migrations
var migrations embed.FS

func Migrate() error {
	_, dbConn, err := Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	goose.SetBaseFS(migrations)

	if err := goose.SetDialect("sqlite3"); err != nil {
		return err
	}

	if err := goose.Up(dbConn, "sqlite/migrations"); err != nil {
		return err
	}

	return nil
}

func NullString(s string) sql.NullString {
	return sql.NullString{
		Valid:  s != "",
		String: s,
	}
}
