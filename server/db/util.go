// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"

	"entanglement.garden/rhyzome/server/config"
)

// Get the database and closable DB object
// example usage:
//
//	  queries, dbConn, err := db.Get()
//	  if err != nil {
//		   return err
//	  }
//	  defer dbConn.Close()
func Get() (*Queries, *sql.DB, error) {
	db, err := sql.Open("sqlite3", config.C.SqlitePath)
	if err != nil {
		return nil, nil, err
	}

	return New(db), db, nil
}
