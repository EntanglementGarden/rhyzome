// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import "entanglement.garden/rhyzome/protos"

func (v Volume) GetGRPC() *protos.Volume {
	return &protos.Volume{
		Id:              v.ID,
		Size:            int32(v.Size),
		SourceImage:     v.SourceImage.String,
		SourceImageQcow: v.SourceImageQcow2.String,
	}
}
