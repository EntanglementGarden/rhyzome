// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"

	"entanglement.garden/rhyzome/protos"
	"entanglement.garden/rhyzome/server/openapi"
)

var (
	instanceStates = map[int64]openapi.InstanceState{
		int64(protos.Instance_PENDING):  openapi.PENDING,
		int64(protos.Instance_CREATING): openapi.CREATING,
		int64(protos.Instance_STARTING): openapi.STARTING,
		int64(protos.Instance_RUNNING):  openapi.RUNNING,
		int64(protos.Instance_STOPPED):  openapi.STOPPED,
		int64(protos.Instance_DELETING): openapi.DELETING,
		int64(protos.Instance_DELETED):  openapi.DELETED,
		int64(protos.Instance_ERROR):    openapi.ERROR,
	}
	knownNetworks = map[string]int32{
		"system": 10,
	}
)

func (i Instance) GetGRPC(ctx context.Context, queries *Queries) (*protos.Instance, error) {
	nics, err := queries.GetNICsByInstance(ctx, i.ID)
	if err != nil {
		logrus.Error("failed to find NICs for instance ", i.ID)
		return nil, err
	}

	nicsProto := []*protos.NetworkInterface{}
	for _, nic := range nics {
		vlansProto := []int32{}
		wan := false
		bootstrap := false

		if nic.Trunk {
			vlans, err := queries.GetTrunkVlans(ctx, nic.ID)
			if err != nil {
				return nil, fmt.Errorf("error finding vlans for trunk port: %v", err)
			}

			for _, vlan := range vlans {
				vlansProto = append(vlansProto, int32(vlan))
			}
		} else if nic.Network == NetworkIDWAN {
			wan = true
		} else if nic.Network == NetworkIDBootstrap {
			bootstrap = true
		} else if vlan, ok := knownNetworks[nic.Network]; ok {
			vlansProto = append(vlansProto, vlan)
		} else {
			vlan, err := queries.GetVlanForNetwork(ctx, nic.Network)
			if err != nil {
				logrus.Error("failed to get vlan for network ", nic.Network)
				return nil, err
			}

			vlansProto = append(vlansProto, int32(vlan))
		}

		nicsProto = append(nicsProto, &protos.NetworkInterface{
			Id:        nic.ID,
			Mac:       nic.Mac,
			Ip:        nic.Ip,
			Vlan:      vlansProto,
			Tagged:    nic.Trunk,
			Wan:       wan,
			Bootstrap: bootstrap,
		})
	}

	logrus.Debug("nic protos: ", nicsProto)

	specialCases, err := queries.GetSpecialCases(ctx, i.ID)
	if err != nil {
		logrus.Error("failed to get special cases for instance ", i.ID)
		return nil, err
	}

	specialCasesProto := []protos.Instance_SpecialCase{}
	for _, c := range specialCases {
		grpcCase, ok := protos.Instance_SpecialCase_value[c]
		if !ok {
			logrus.Warn("unexpected special case cannot be converted to gRPC: ", c)
			continue
		}

		specialCasesProto = append(specialCasesProto, protos.Instance_SpecialCase(grpcCase))
	}

	return &protos.Instance{
		Id:                i.ID,
		State:             protos.Instance_State(i.State),
		RootVolume:        i.RootVolume,
		Memory:            int32(i.Memory),
		Cores:             int32(i.Cpu),
		NetworkInterfaces: nicsProto,
		Biosdata:          i.BiosData.String,
		SpecialCases:      specialCasesProto,
	}, nil
}

func (i Instance) GetOpenAPI(ctx context.Context, queries *Queries) (*openapi.Instance, error) {
	nics, err := queries.GetNICsByInstance(ctx, i.ID)
	if err != nil {
		return nil, err
	}

	nicsOpenAPI := []openapi.NetworkInterface{}
	for _, nic := range nics {
		openapiNic := openapi.NetworkInterface{
			Id:      nic.ID,
			Mac:     nic.Mac,
			Network: nic.Network,
		}

		if nic.Trunk {
			openapiNic.Trunk = &nic.Trunk
		} else if len(nic.Ip) > 0 { // WAN interfaces are non-trunk but also have no IP in the DB (currently)
			openapiNic.Addresses = []string{nic.Ip}
		}

		nicsOpenAPI = append(nicsOpenAPI, openapiNic)
	}

	logrus.Debug("found ", len(nicsOpenAPI), " NICs for instance")

	userData := string(i.UserData)

	return &openapi.Instance{
		ResourceId: i.ID,
		State:      instanceStates[i.State],
		UserData:   &userData,
		RootVolume: i.RootVolume,
		Memory:     int(i.Memory),
		Cpu:        int(i.Cpu),
		Interfaces: nicsOpenAPI,
	}, nil
}
