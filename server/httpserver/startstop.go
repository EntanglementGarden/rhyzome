// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"context"
	"fmt"
	"net/http"

	echo "github.com/labstack/echo/v4"

	"entanglement.garden/rhyzome/protos"
	"entanglement.garden/rhyzome/server/db"
	"entanglement.garden/rhyzome/server/grpcserver"
)

func (server) StartInstance(c echo.Context, instanceID string) error {
	if err := setInstanceState(c.Request().Context(), instanceID, protos.Instance_STARTING); err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

func (server) StopInstance(c echo.Context, instanceID string) error {
	if err := setInstanceState(c.Request().Context(), instanceID, protos.Instance_STOPPING); err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

func (server) RebootInstance(c echo.Context, instanceID string) error {
	if err := setInstanceState(c.Request().Context(), instanceID, protos.Instance_REBOOTING); err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

func setInstanceState(ctx context.Context, instanceID string, state protos.Instance_State) error {
	queries, dbConn, err := db.Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	instance, err := queries.GetInstance(ctx, instanceID)
	if err != nil {
		return fmt.Errorf("error looking up instance: %v", err)
	}

	instance.State = int64(state)

	err = queries.SetInstanceState(ctx,
		db.SetInstanceStateParams{
			ID:    instanceID,
			State: instance.State,
		},
	)
	if err != nil {
		return err
	}

	grpcInstance, err := instance.GetGRPC(ctx, queries)
	if err != nil {
		return err
	}

	grpcserver.PushEvents(ctx, instance.Node.String, &protos.Event{Instance: grpcInstance})

	return nil
}
