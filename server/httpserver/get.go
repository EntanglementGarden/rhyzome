// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"database/sql"
	"fmt"
	"net/http"

	echo "github.com/labstack/echo/v4"

	"entanglement.garden/rhyzome/server/db"
)

func (server) GetInstance(e echo.Context, id string) error {
	queries, dbConn, err := db.Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	instance, err := queries.GetInstance(e.Request().Context(), id)
	if err != nil {
		if err == sql.ErrNoRows {
			return e.JSON(http.StatusNotFound, map[string]string{"status": "instance not found"})
		}
		return fmt.Errorf("error getting instance state: %v", err)
	}

	resp, err := instance.GetOpenAPI(e.Request().Context(), queries)
	if err != nil {
		return err
	}

	return e.JSON(http.StatusOK, resp)
}
