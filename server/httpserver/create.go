// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"database/sql"
	"encoding/base64"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	echo "github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"entanglement.garden/api-client/egapi"
	"entanglement.garden/api-client/egnetworking"
	"entanglement.garden/common/httpx"
	"entanglement.garden/common/identifiers"
	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/protos"
	"entanglement.garden/rhyzome/server/db"
	"entanglement.garden/rhyzome/server/grpcserver"
	"entanglement.garden/rhyzome/server/openapi"
)

func (server) CreateInstance(c echo.Context) error {
	ctx := c.Request().Context()
	log := logging.GetLog(ctx)

	log.Info("creating instance")

	var req openapi.CreateInstanceRequestBody
	if err := c.Bind(&req); err != nil {
		return err
	}

	if req.BaseImage == "" && req.BaseImageQcow2 == "" {
		return c.JSON(400, map[string]string{"error": "invalid request: base_image or base_image_qcow2 missing"})
	}

	if req.Memory == 0 {
		return c.JSON(400, map[string]string{"error": "invalid request: memory missing"})
	}

	if req.Cpu == 0 {
		return c.JSON(400, map[string]string{"error": "invalid request: cpu missing"})
	}

	if req.DiskSize == 0 {
		return c.JSON(400, map[string]string{"error": "invalid request: disk_size missing"})
	}

	queries, dbConn, err := db.Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	instanceID := identifiers.GenerateID(identifiers.PrefixInstance)
	volumeID := identifiers.GenerateID(identifiers.PrefixVolume)

	userData := ""
	if req.UserData != nil {
		userData = base64.StdEncoding.EncodeToString([]byte(*req.UserData))
	}

	err = queries.CreateVolume(ctx, db.CreateVolumeParams{
		ID:               volumeID,
		Tenant:           httpx.GetTenant(c),
		Size:             int64(req.DiskSize),
		SourceImage:      db.NullString(req.BaseImage),
		SourceImageQcow2: db.NullString(req.BaseImageQcow2),
	})
	if err != nil {
		return fmt.Errorf("sql error creating volume: %v", err)
	}

	biosdata := sql.NullString{Valid: false}
	specialCases := map[string]bool{}
	if req.SpecialCases != nil {
		for _, s := range *req.SpecialCases {
			_, ok := protos.Instance_SpecialCase_value[s]
			if !ok {
				return fmt.Errorf("unknown instance special case: %s", s)
			}

			specialCases[s] = true

			switch s {
			case protos.Instance_ROUTER.String():
				data, err := getRouterBIOSData(c, instanceID)
				if err != nil {
					return err
				}

				biosdata.Valid = data != ""
				biosdata.String = data
			}
		}
	}

	instance, err := queries.CreateInstance(ctx, db.CreateInstanceParams{
		ID:         instanceID,
		Tenant:     httpx.GetTenant(c),
		Name:       instanceID,
		Memory:     int64(req.Memory),
		Cpu:        int64(req.Cpu),
		UserData:   []byte(userData),
		RootVolume: volumeID,
		BiosData:   biosdata,
	})
	if err != nil {
		return fmt.Errorf("sql error creating instance: %v", err)
	}

	if req.SpecialCases != nil {
		for _, s := range *req.SpecialCases {
			err = queries.SetSpecialCase(ctx, db.SetSpecialCaseParams{
				Instance:    instanceID,
				SpecialCase: s,
			})
			if err != nil {
				return err
			}
		}
	}

	log.Debug("added instance to DB")

	if len(req.Networks) == 0 {
		log.Warn("creating instance with no networks attached")
	}

	egClient, err := egapi.NewInternalClientFromRequest(c.Request())
	if err != nil {
		return fmt.Errorf("error creating API client: %v", err)
	}

	oapiInterfaces := []openapi.NetworkInterface{}
	trunk := false

	if _, isRouter := specialCases[protos.Instance_ROUTER.String()]; isRouter {
		// WAN interface
		oapi, err := generateNIC(c, egClient, queries, db.NetworkIDWAN, instanceID, userData, trunk)
		if err != nil {
			return fmt.Errorf("error creating network interface: %v", err)
		}
		oapiInterfaces = append(oapiInterfaces, oapi)
		trunk = true
	}

	for _, network := range req.Networks {
		oapi, err := generateNIC(c, egClient, queries, network, instanceID, userData, trunk)
		if err != nil {
			return fmt.Errorf("error creating network interface: %v", err)
		}
		oapiInterfaces = append(oapiInterfaces, oapi)
	}

	if _, isBootstrapNetwork := specialCases[protos.Instance_ROUTER.String()]; isBootstrapNetwork {
		// bootstrap interface
		oapi, err := generateNIC(c, egClient, queries, db.NetworkIDBootstrap, instanceID, "", false)
		if err != nil {
			return fmt.Errorf("error creating network interface: %v", err)
		}
		oapiInterfaces = append(oapiInterfaces, oapi)
	}

	// TODO: assign to node after responding to API request. do not fail API request if no nodes are available.
	nodes, err := queries.GetAvailableNodes(ctx)
	if err != nil {
		return err
	}

	var nodePtr *string

	nodeCount := len(nodes)
	if nodeCount > 0 {
		node := nodes[rand.Intn(nodeCount)]
		log.Debug("assigned to node ", node, " (", nodeCount, " total nodes)")
		nodePtr = &node

		err = queries.AssignVolumeToNode(ctx, db.AssignVolumeToNodeParams{
			ID:   volumeID,
			Node: sql.NullString{String: node, Valid: true},
		})
		if err != nil {
			return err
		}

		err = queries.AssignInstanceToNode(ctx, db.AssignInstanceToNodeParams{
			ID:   instanceID,
			Node: sql.NullString{String: node, Valid: true},
		})
		if err != nil {
			return err
		}

		grpcInstance, err := instance.GetGRPC(ctx, queries)
		if err != nil {
			return fmt.Errorf("error converting instance to gRPC: %v", err)
		}

		grpcserver.PushEvents(ctx, node,
			&protos.Event{
				Volume: &protos.Volume{
					Id:              volumeID,
					Size:            int32(req.DiskSize),
					SourceImage:     req.BaseImage,
					SourceImageQcow: req.BaseImageQcow2,
				},
				Instance: grpcInstance,
			})
	}

	return c.JSON(http.StatusCreated, openapi.Instance{
		ResourceId: instanceID,
		Interfaces: oapiInterfaces,
		Memory:     req.Memory,
		Node:       nodePtr,
		RootVolume: volumeID,
		State:      openapi.PENDING,
		UserData:   req.UserData,
	})
}

func getRouterBIOSData(c echo.Context, hostname string) (string, error) {
	ctx := c.Request().Context()
	log := logging.GetLog(ctx)

	egClient, err := egapi.NewInternalClientFromRequest(c.Request())
	if err != nil {
		return "", fmt.Errorf("error creating API client: %v", err)
	}

	networking, err := egnetworking.NewClient(egClient)
	if err != nil {
		return "", err
	}

	for {
		resp, err := networking.GenerateRouterBootDataWithResponse(ctx, egnetworking.RouterBootDataRequest{
			Hostname: hostname,
		})
		if err != nil {
			log.Error("unexpected error requesting PKI token (will retry): ", err)
			time.Sleep(time.Second)
			continue
		}

		if resp.JSON201 == nil {
			logging.GetLog(ctx).WithFields(logrus.Fields{
				"req_url": resp.HTTPResponse.Request.URL,
				"status":  resp.Status(),
			}).Debug("unexpected response status")
			log.Errorf("unexpected %s response while requesting PKI token: %s", resp.Status(), string(resp.Body))
			time.Sleep(time.Second)
			continue
		}

		return resp.JSON201.Biosdata, nil
	}
}
