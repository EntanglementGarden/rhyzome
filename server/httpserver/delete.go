// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"errors"
	"fmt"
	"net/http"

	echo "github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"entanglement.garden/rhyzome/protos"
	"entanglement.garden/rhyzome/server/db"
	"entanglement.garden/rhyzome/server/grpcserver"
)

func (server) DeleteInstance(e echo.Context, id string) error {
	ctx := e.Request().Context()

	if id == installData.SystemInstanceID {
		return errors.New("deleting service instance is blocked for now")
	}

	logrus.Info("deleting instance")

	queries, dbConn, err := db.Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	instance, err := queries.GetInstance(ctx, id)
	if err != nil {
		return fmt.Errorf("error looking up instance: %v", err)
	}

	err = queries.SetInstanceState(ctx, db.SetInstanceStateParams{
		State: int64(protos.Instance_DELETING),
		ID:    id,
	})
	if err != nil {
		return err
	}

	err = queries.SetVolumeState(ctx, db.SetVolumeStateParams{
		State: int64(protos.Volume_DELETED),
		ID:    instance.RootVolume,
	})
	if err != nil {
		return err
	}

	grpcserver.PushEvents(ctx, instance.Node.String,
		&protos.Event{
			Instance: &protos.Instance{
				Id:    id,
				State: protos.Instance_DELETING,
			},
		},
		&protos.Event{
			Volume: &protos.Volume{
				Id:    instance.RootVolume,
				State: protos.Volume_DELETED,
			},
		})

	return e.NoContent(http.StatusNoContent)
}
