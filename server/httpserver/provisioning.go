// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"crypto/sha256"
	"encoding/pem"
	"fmt"
	"io"
	"net/http"
	"os"

	echo "github.com/labstack/echo/v4"

	"entanglement.garden/common/cluster"
	"entanglement.garden/rhyzome/server/config"
)

// ProvisioningEndpoints defines model for ProvisioningEndpoints.
type ProvisioningEndpoints struct {
	LibvirtGRPC string `json:"libvirt_grpc"`
	OpenWRTGRPC string `json:"openwrt_grpc"`
	Step        string `json:"step"`
}

// ProvisioningInfo defines model for ProvisioningInfo.
type ProvisioningInfo struct {
	CAFingerprint string                `json:"ca_fingerprint"` // TODO: see if this can be removed
	Domain        string                `json:"domain"`
	Endpoints     ProvisioningEndpoints `json:"endpoints"`
}

// provisioning endpoint provides information about the cluster that is used for service discovery
// it lives in rhyzome-libvirt because it's used by the bootstrapping process before anything else comes up.
func provisioning(c echo.Context) error {
	f, err := os.Open(config.C.PKI.CA)
	if err != nil {
		return err
	}
	defer f.Close()

	pemstr, err := io.ReadAll(f)
	if err != nil {
		return err
	}

	ca, _ := pem.Decode(pemstr)

	fp := sha256.Sum256(ca.Bytes)

	return c.JSON(http.StatusOK, ProvisioningInfo{
		CAFingerprint: fmt.Sprintf("%x", fp),
		Domain:        cluster.Domain,
		Endpoints: ProvisioningEndpoints{
			LibvirtGRPC: fmt.Sprintf("entanglement-garden-rhyzome.%s:%d", cluster.Domain, config.C.GRPCPort),
			OpenWRTGRPC: fmt.Sprintf("entanglement-garden-networking.%s:9091", cluster.Domain),
			Step:        fmt.Sprintf("https://ca.%s:8443", cluster.Domain),
		},
	})
}
