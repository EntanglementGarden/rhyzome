package httpserver

import (
	"database/sql"
	"errors"
	"fmt"

	echo "github.com/labstack/echo/v4"

	"entanglement.garden/api-client/egapi"
	"entanglement.garden/api-client/egnetworking"
	"entanglement.garden/rhyzome/server/db"
)

func ensureNetwork(c echo.Context, queries *db.Queries, networkID string) error {
	// don't do anything for the bootstrap network
	if networkID == db.NetworkIDBootstrap {
		return nil
	}

	ctx := c.Request().Context()

	_, err := queries.GetVlanForNetwork(ctx, networkID)
	if err != sql.ErrNoRows {
		return nil
	}

	vlan := 0

	if networkID != db.NetworkIDWAN {
		client, err := egapi.NewInternalClientFromRequest(c.Request())
		if err != nil {
			return err
		}

		networking, err := egnetworking.NewClient(client)
		if err != nil {
			return err
		}

		resp, err := networking.GetNetworkWithResponse(ctx, networkID)
		if err != nil {
			return err
		}

		if resp.JSON200 == nil {
			return fmt.Errorf("error getting network vlan: %s %s", resp.Status(), resp.Body)
		}

		if resp.JSON200.Vlan == nil {
			return errors.New("no vlan returned from network create request")
		}

		vlan = *resp.JSON200.Vlan
	}

	err = queries.AddNetwork(ctx, db.AddNetworkParams{
		ID:   networkID,
		Vlan: int64(vlan),
	})
	if err != nil {
		return err
	}

	return nil
}
