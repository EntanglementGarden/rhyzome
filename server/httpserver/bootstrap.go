// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"database/sql"
	"fmt"
	"net/http"

	echo "github.com/labstack/echo/v4"

	"entanglement.garden/common/installdata"
	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/protos"
	"entanglement.garden/rhyzome/server/db"
	"entanglement.garden/rhyzome/server/grpcserver"
)

var installData = func() installdata.InstallData {
	installData, err := installdata.ReadInstallData()
	if err != nil {
		panic(fmt.Errorf("error reading install data: %v", err))
	}
	return installData
}()

func (server) BootstrapSystemNetworkReady(c echo.Context) error {
	ctx := c.Request().Context()
	log := logging.GetLog(ctx)

	log.Debug("notified that system network is ready")

	var ip string
	if err := c.Bind(&ip); err != nil {
		return err
	}

	queries, dbConn, err := db.Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	log.Debug("checking if the system VM already has a NIC")
	_, err = queries.GetNICIP(ctx, installData.SystemInstanceNIC)
	if err != nil && err != sql.ErrNoRows {
		log.Error("error checking system VM NIC")
		return err
	}
	if err == nil {
		log.Info("NIC already exists, taking no action")
		return c.NoContent(http.StatusNoContent)
	}

	log.Debug("creating NIC on system network for system VM")
	err = queries.CreateNIC(ctx, db.CreateNICParams{
		ID:       installData.SystemInstanceNIC,
		Instance: installData.SystemInstanceID,
		Mac:      installData.SystemInstanceMAC,
		Ip:       ip,
		Network:  "system",
		Trunk:    false,
	})
	if err != nil {
		return err
	}

	log.Debug("pushing update via grpc")
	instance, err := queries.GetInstance(ctx, installData.SystemInstanceID)
	if err != nil {
		return fmt.Errorf("error looking up instance: %v", err)
	}

	grpcInstance, err := instance.GetGRPC(ctx, queries)
	if err != nil {
		return err
	}

	grpcserver.PushEvents(ctx, instance.Node.String, &protos.Event{Instance: grpcInstance})

	log.Debug("done adding system NIC to system VM")
	return c.NoContent(http.StatusNoContent)
}

func (server) BootstrapComplete(c echo.Context) error {
	ctx := c.Request().Context()
	log := logging.GetLog(ctx)

	log.Debug("notified that bootstrapping is complete, removing bootstrap network")

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	// remove all NICs that are on the bootstrap network
	instances, err := queries.RemoveAllNicsOnNetwork(ctx, db.NetworkIDBootstrap)
	if err != nil {
		return fmt.Errorf("error removing NICs on bootstrap network: %v", err)
	}

	for _, instanceID := range instances {
		instance, err := queries.GetInstance(ctx, instanceID)
		if err != nil {
			return fmt.Errorf("failed to look up instance: %v", err)
		}

		instanceProto, err := instance.GetGRPC(ctx, queries)
		if err != nil {
			return fmt.Errorf("failed to convert instance to grpc: %v", err)
		}

		grpcserver.PushEvents(ctx, instance.Node.String, &protos.Event{
			Instance:          instanceProto,
			BootstrapComplete: len(instanceProto.NetworkInterfaces) == 1, // this is a very janky way to detect the services VM vs the router
		})
	}

	return c.NoContent(http.StatusNoContent)
}
