// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"

	echo "github.com/labstack/echo/v4"

	"entanglement.garden/api-client/egapi"
	"entanglement.garden/api-client/egnetworking"
	"entanglement.garden/common/identifiers"
	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/common"
	"entanglement.garden/rhyzome/protos"
	"entanglement.garden/rhyzome/server/config"
	"entanglement.garden/rhyzome/server/db"
	"entanglement.garden/rhyzome/server/grpcserver"
	"entanglement.garden/rhyzome/server/openapi"
)

func (server) TrunkNicAddNetwork(c echo.Context, instanceID string, nic string) error {
	var network openapi.TrunkNicAddNetworkJSONBody
	if err := c.Bind(&network); err != nil {
		return err
	}

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	ctx := c.Request().Context()

	instance, err := queries.GetInstance(ctx, instanceID)
	if err != nil {
		if err == sql.ErrNoRows {
			return c.JSON(http.StatusNotFound, map[string]string{"error": "instance not found"})
		}
	}

	err = ensureNetwork(c, queries, network)
	if err != nil {
		return fmt.Errorf("error getting network vlan: %v", err)
	}

	err = queries.AddTrunkNetwork(ctx, db.AddTrunkNetworkParams{
		Nic:     nic,
		Network: network,
	})
	if err != nil {
		return err
	}

	if instance.Node.Valid {
		grpcInstance, err := instance.GetGRPC(ctx, queries)
		if err != nil {
			return err
		}

		grpcserver.PushEvents(ctx, instance.Node.String, &protos.Event{Instance: grpcInstance})
	}

	return c.NoContent(http.StatusNoContent)
}

func (server) AttachNic(c echo.Context, instanceID string) error {
	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	var networkID string
	if err := c.Bind(&networkID); err != nil {
		return err
	}

	ctx := c.Request().Context()

	instance, err := queries.GetInstance(ctx, instanceID)
	if err == sql.ErrNoRows {
		return c.JSON(http.StatusNotFound, map[string]string{"error": "instance not found"})
	} else if err != nil {
		return err
	}

	_, err = queries.GetNetworkByID(ctx, networkID)
	if err == sql.ErrNoRows {
		return c.JSON(http.StatusNotFound, map[string]string{"error": "network not found"})
	} else if err != nil {
		return err
	}

	client, err := egapi.NewInternalClientFromRequest(c.Request())
	if err != nil {
		return fmt.Errorf("error creating API client: %v", err)
	}

	resp, err := generateNIC(c, client, queries, networkID, instanceID, string(instance.UserData), false)
	if err != nil {
		return fmt.Errorf("error generating NIC: %v", err)
	}

	if instance.Node.Valid {
		grpcInstance, err := instance.GetGRPC(ctx, queries)
		if err != nil {
			return err
		}

		grpcserver.PushEvents(ctx, instance.Node.String, &protos.Event{Instance: grpcInstance})
	}

	return c.JSON(http.StatusCreated, resp)
}

func (server) RemoveNic(c echo.Context, instanceID string, nicID string) error {
	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	ctx := c.Request().Context()

	// verify the instance exists
	instance, err := queries.GetInstance(ctx, instanceID)
	if err != nil {
		if err == sql.ErrNoRows {
			return c.JSON(http.StatusNotFound, map[string]string{"error": "instance not found"})
		}
		return err
	}

	err = queries.DeleteNIC(ctx, db.DeleteNICParams{
		Instance: instanceID,
		ID:       nicID,
	})
	if err != nil {
		return err
	}

	if instance.Node.Valid {
		grpcInstance, err := instance.GetGRPC(ctx, queries)
		if err != nil {
			return err
		}

		grpcserver.PushEvents(ctx, instance.Node.String, &protos.Event{Instance: grpcInstance})
	}

	logging.GetLog(ctx).Debug("NIC removed")

	return c.NoContent(http.StatusNoContent)
}

func generateNIC(c echo.Context, client *egapi.Client, queries *db.Queries, network string, instanceID string, userData string, trunk bool) (openapi.NetworkInterface, error) {
	ctx := c.Request().Context()

	nicID := identifiers.GenerateID(identifiers.PrefixInstanceNic)

	mac, err := common.GenerateMAC(config.C.MACPrefix)
	if err != nil {
		return openapi.NetworkInterface{}, fmt.Errorf("error generating MAC: %v", err)
	}

	ip := ""

	err = ensureNetwork(c, queries, network)
	if err != nil {
		return openapi.NetworkInterface{}, err
	}

	if network != db.NetworkIDWAN && network != db.NetworkIDBootstrap {
		networking, err := egnetworking.NewClient(client)
		if err != nil {
			return openapi.NetworkInterface{}, err
		}

		resp, err := networking.AddHostWithResponse(ctx, network, egnetworking.AddHostBody{
			Id:       nicID,
			Instance: instanceID,
			Mac:      mac,
			UserData: userData,
		})
		if err != nil {
			return openapi.NetworkInterface{}, fmt.Errorf("error registering host with network service: %v", err)
		}

		if resp.JSON201 == nil {
			return openapi.NetworkInterface{}, fmt.Errorf("error %s from networking service: %s", resp.HTTPResponse.Status, string(resp.Body))
		}

		if !trunk {
			if len(resp.JSON201.Addresses) < 1 {
				return openapi.NetworkInterface{}, errors.New("networking service returned no IP addresses for new NIC")
			}

			if len(resp.JSON201.Addresses) != 1 {
				logging.GetLog(ctx).Warn("Unexpected number of IP addresses returned while adding NIC. Only adding first one, if this is an issue please add support for multiple IPs per NIC to rhyzome-libvirt")
			}

			ip = resp.JSON201.Addresses[0]
		}
	}

	err = queries.CreateNIC(ctx, db.CreateNICParams{
		ID:       nicID,
		Instance: instanceID,
		Mac:      mac,
		Ip:       ip,
		Network:  network,
		Trunk:    trunk,
	})
	if err != nil {
		return openapi.NetworkInterface{}, err
	}

	if trunk {
		err = queries.AddTrunkNetwork(ctx, db.AddTrunkNetworkParams{
			Nic:     nicID,
			Network: network,
		})
		if err != nil {
			return openapi.NetworkInterface{}, err
		}
	}

	oapi := openapi.NetworkInterface{
		Id:      nicID,
		Mac:     mac,
		Network: network,
	}

	if trunk {
		oapi.Trunk = &trunk
	}

	return oapi, nil
}
