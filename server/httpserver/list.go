// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"fmt"

	echo "github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"entanglement.garden/common/httpx"
	"entanglement.garden/rhyzome/server/db"
	"entanglement.garden/rhyzome/server/openapi"
)

func (server) ListAllInstances(e echo.Context) error {
	queries, dbConn, err := db.Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	tenant := httpx.GetTenant(e)
	instances, err := queries.ListInstances(e.Request().Context(), tenant)
	if err != nil {
		logrus.Error("error listing instances: ", err)
		return err
	}

	response := []openapi.Instance{}
	for _, instance := range instances {
		instanceResponse, err := instance.GetOpenAPI(e.Request().Context(), queries)
		if err != nil {
			return fmt.Errorf("error preparing instance details: %v", err)
		}

		response = append(response, *instanceResponse)
	}

	return e.JSON(200, response)
}
