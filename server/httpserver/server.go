// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"context"
	_ "embed"
	"net/http"

	echo "github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"entanglement.garden/common/httpx"
	"entanglement.garden/rhyzome/server/config"
	"entanglement.garden/rhyzome/server/openapi"
)

type server struct{}

var (
	//go:embed web.ewp
	ewp []byte

	e *echo.Echo
)

func Serve() {
	e = httpx.NewServerWithEWP(ewp)

	openapi.RegisterHandlersWithBaseURL(e, &server{}, "/entanglement.garden/rhyzome")

	e.File(".well-known/rhyzome/ca.pem", config.C.PKI.CA)
	e.GET(".well-known/rhyzome/provisioning.json", provisioning)

	logrus.Info("starting http listener on ", config.C.HTTPBind)

	err := e.Start(config.C.HTTPBind)
	if err != http.ErrServerClosed {
		logrus.Fatal(err)
	}
}

func Shutdown() {
	if e != nil {
		logrus.Info("shutting down http listener")
		err := e.Shutdown(context.Background())
		if err != nil {
			logrus.Error("error shutting down http server: ", err)
		}
	}
}
