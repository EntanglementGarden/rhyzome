// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	"github.com/sirupsen/logrus"

	"entanglement.garden/common/config"
	"entanglement.garden/common/grpcx"
)

// Config describes all configurable keys
type Config struct {
	// Server options
	HTTPBind    string `json:"http_bind,omitempty"`
	GRPCPort    int    `json:"grpc_port,omitempty"`
	SqlitePath  string `json:"sqlite_path,omitempty"`  // SqlitePath is the path to the sqlite database file
	ImageFormat string `json:"image_format,omitempty"` // ImageFormat is `qcow2` if qcow2 files should be uploaded to created volumes, otherwise `raw`

	// shared
	MACPrefix string     `json:"mac_prefix,omitempty"`
	PKI       *grpcx.PKI `json:"pki,omitempty"` // used for gRPC mTLS
	Domain    string     `json:"domain"`        // the system's root domain
}

var (
	// C stores the actual configured values, defaults shown below
	C = Config{
		ImageFormat: "qcow2",
		HTTPBind:    ":8080",
		GRPCPort:    9090,
		SqlitePath:  "/var/rhyzome/rhyzome.db",
		MACPrefix:   "52:54:00", // 52:54:00 is libvirt's mac prefix
		PKI:         grpcx.NewServerPKI(),
	}
)

// Load loads the configuration off the disk
func Load() {
	if err := config.Load("entanglement.garden", "rhyzome", &C); err != nil {
		logrus.Fatal("error loading config: ", err)
	}
}

func GetFakeHosts() []string {
	return []string{"ca." + C.Domain, "entanglement-garden-rhyzome." + C.Domain}
}
