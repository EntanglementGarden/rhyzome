// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package serial

import (
	"io"

	"github.com/sirupsen/logrus"

	"entanglement.garden/rhyzome/client/instances"
	"entanglement.garden/rhyzome/client/libvirtx"
)

func Log(instanceID string) {
	logger := logrus.WithField("instance", instanceID)

	conn, err := libvirtx.New()
	if err != nil {
		logger.Error("error connecting to libvirt: ", err)
		return
	}
	defer conn.Close()

	dom, err := conn.LookupDomainByName(instances.IDToDomainName(instanceID))
	if err != nil {
		logger.Error("error finding domain: ", err)
		return
	}
	defer libvirtx.Free(dom)

	stream, err := conn.NewStream(0)
	if err != nil {
		logger.Error("error setting up stream: ", err)
		return
	}
	defer libvirtx.Free(stream)

	streamReader := libvirtx.Stream{Stream: stream}
	defer streamReader.Close()

	err = dom.OpenConsole("", stream, 0)
	if err != nil {
		logger.Error("error opening console")
		return
	}

	_, err = io.Copy(logger.WriterLevel(logrus.DebugLevel), streamReader)
	if err != nil {
		logrus.Error("error reading from stream: ", err)
		return
	}
}
