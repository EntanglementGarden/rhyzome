// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"

	"entanglement.garden/common/config"
	"entanglement.garden/common/grpcx"
)

// Config describes all configurable keys
type Config struct {
	// agent options
	BridgeInterface string `json:"bridge_interface,omitempty"`  // BridgeInterface was the bridge that all network interfaces are added to and is still used for some things
	SharedDir       string `json:"shared_dir,omitempty"`        // SharedDir must be accessible by the rhyzome-libvirt agent as well as the libvirt hooks
	WANBridge       string `json:"wan_interface,omitempty"`     // WANBridge is the bridge interface on the host to use for WAN interfaces on routers. Ignored if WANNetwork is specified.
	ImageDir        string `json:"image_dir,omitempty"`         // ImageDir is the path to the local image pool
	DiskStoragePool string `json:"disk_storage_pool,omitempty"` // DiskStoragePool is the name of the storage pool to use
	ImageCache      string `json:"image_cache,omitempty"`       // ImageCache is the path on the local disk to a folder where images should be cached
	ImageHost       string `json:"imagehost,omitempty"`         // ImageHost is the base URL for the disk image server
	MetadataURL     string `json:"metadata_url,omitempty"`
	APIServer       string `json:"apiserver,omitempty"` // The host:port of the gRPC server to connect to

	// shared
	MACPrefix string    `json:"mac_prefix,omitempty"`
	PKI       grpcx.PKI `json:"pki,omitempty"` // used for gRPC mTLS
	Domain    string    `json:"domain"`        // the system's root domain
}

var (
	// C stores the actual configured values, defaults shown below
	C = Config{
		BridgeInterface: "br0",
		WANBridge:       "brwan",
		DiskStoragePool: "default",
		ImageDir:        "/var/lib/libvirt/images",
		ImageHost:       "http://image-host.fruit-0.entanglement.garden",
		ImageCache:      fmt.Sprintf("%s/.cache/rhyzome/images/", os.Getenv("HOME")),
		MetadataURL:     "http://169.254.169.254",
		MACPrefix:       "52:54:00", // 52:54:00 is libvirt's mac prefix
		SharedDir:       "/var/run/rhyzome-agent",
	}
)

// Load loads the configuration off the disk
func Load() {
	if err := config.Load("entanglement.garden", "rhyzome", &C); err != nil {
		logrus.Fatal("error loading config: ", err)
	}
}

func GetFakeHosts() []string {
	return []string{"ca." + C.Domain, "entanglement-garden-rhyzome." + C.Domain}
}
