// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
	"libvirt.org/go/libvirt"
	"libvirt.org/go/libvirtxml"

	"entanglement.garden/rhyzome/client/config"
	"entanglement.garden/rhyzome/client/volumes/pools"
	"entanglement.garden/rhyzome/common"
	"entanglement.garden/rhyzome/protos"
)

func create(conn *libvirt.Connect, instance *protos.Instance) error {
	logger := logrus.WithFields(logrus.Fields{"instance": instance.Id})
	logger.Info("creating instance")

	logger.Debug("getting storage pool")
	pool, err := pools.GetPool(conn)
	if err != nil {
		return err
	}

	logger.Debug("preparing domain XML")
	disks := []libvirtxml.DomainDisk{pool.GetDomainDiskXML(instance.RootVolume)}
	for _, vol := range instance.Volumes {
		disks = append(disks, pool.GetDomainDiskXML(vol))
	}

	smbios := map[int]map[string]string{
		1: {"serial": fmt.Sprintf("ds=nocloud-net;s=%s/", config.C.MetadataURL)},
		3: {"manufacturer": "entanglement.garden"},
	}

	if instance.Biosdata != "" {
		smbios[1]["serial"] = instance.Biosdata
	}

	qemuArgs := []libvirtxml.DomainQEMUCommandlineArg{}
	for smbiosType, values := range smbios {
		arg := libvirtxml.DomainQEMUCommandlineArg{
			Value: fmt.Sprintf("type=%d", smbiosType),
		}
		for key, value := range values {
			arg.Value = fmt.Sprintf("%s,%s=%s", arg.Value, key, value)
		}
		qemuArgs = append(qemuArgs, libvirtxml.DomainQEMUCommandlineArg{Value: "-smbios"}, arg)
	}

	interfaces := []libvirtxml.DomainInterface{}
	firewallInfo := AllNICs{}
	for _, nic := range instance.NetworkInterfaces {
		iface := libvirtxml.DomainInterface{
			Model: &libvirtxml.DomainInterfaceModel{Type: "virtio"},
			MAC:   &libvirtxml.DomainInterfaceMAC{Address: nic.Mac},
		}

		switch {
		case nic.Wan:
			if strings.HasPrefix(config.C.WANBridge, "network:") { // special case to allow WAN to be a libvirt network
				iface.Source = &libvirtxml.DomainInterfaceSource{
					Network: &libvirtxml.DomainInterfaceSourceNetwork{
						Network: strings.TrimPrefix(config.C.WANBridge, "network:"),
					},
				}
			} else {
				iface.Source = &libvirtxml.DomainInterfaceSource{
					Bridge: &libvirtxml.DomainInterfaceSourceBridge{
						Bridge: config.C.WANBridge,
					},
				}
			}

		case nic.Bootstrap:
			iface.Source = &libvirtxml.DomainInterfaceSource{
				Network: &libvirtxml.DomainInterfaceSourceNetwork{
					Network: common.NetworkIDBootstrap,
				},
			}

		default:
			iface.Source = &libvirtxml.DomainInterfaceSource{
				Bridge: &libvirtxml.DomainInterfaceSourceBridge{Bridge: config.C.BridgeInterface},
			}

			iface.VirtualPort = &libvirtxml.DomainInterfaceVirtualPort{
				Params: &libvirtxml.DomainInterfaceVirtualPortParams{
					OpenVSwitch: &libvirtxml.DomainInterfaceVirtualPortParamsOpenVSwitch{},
				},
			}

			iface.VLan = generateXMLVlan(nic)
		}

		interfaces = append(interfaces, iface)

		if nic.Tagged || nic.Wan || nic.Ip == "" { // don't filter tagged NICs or WAN NICs, they're probably routers. We should have a way to explicitly request this
			firewallInfo[nic.Mac] = NICFilters{Unfiltered: true}
		} else {
			firewallInfo[nic.Mac] = NICFilters{IPs: []string{nic.Ip}}
		}
	}

	description, err := json.Marshal(firewallInfo)
	if err != nil {
		return err
	}

	// Create the domain
	domainXML := &libvirtxml.Domain{
		Type:        "kvm",
		Name:        IDToDomainName(instance.Id),
		Memory:      &libvirtxml.DomainMemory{Value: uint(instance.Memory), Unit: "MiB"},
		VCPU:        &libvirtxml.DomainVCPU{Value: uint(instance.Cores)},
		Description: string(description),
		OS: &libvirtxml.DomainOS{
			Type:        &libvirtxml.DomainOSType{Arch: "x86_64", Type: "hvm"},
			BootDevices: []libvirtxml.DomainBootDevice{{Dev: "hd"}},
		},
		Features: &libvirtxml.DomainFeatureList{
			ACPI:   &libvirtxml.DomainFeature{},
			APIC:   &libvirtxml.DomainFeatureAPIC{},
			VMPort: &libvirtxml.DomainFeatureState{State: "off"},
		},
		CPU: &libvirtxml.DomainCPU{Mode: "host-model"},
		Devices: &libvirtxml.DomainDeviceList{
			Emulator: "/usr/bin/kvm",
			Disks:    disks,
			Channels: []libvirtxml.DomainChannel{
				{
					Source: &libvirtxml.DomainChardevSource{
						UNIX: &libvirtxml.DomainChardevSourceUNIX{Path: "/var/lib/libvirt/qemu/f16x86_64.agent", Mode: "bind"},
					},
					Target: &libvirtxml.DomainChannelTarget{
						VirtIO: &libvirtxml.DomainChannelTargetVirtIO{Name: "org.qemu.guest_agent.0"},
					},
				},
			},
			Consoles:   []libvirtxml.DomainConsole{{Target: &libvirtxml.DomainConsoleTarget{}}},
			Serials:    []libvirtxml.DomainSerial{{}},
			Interfaces: interfaces,
		},
		QEMUCommandline: &libvirtxml.DomainQEMUCommandline{Args: qemuArgs},
	}

	domainXMLString, err := domainXML.Marshal()
	if err != nil {
		return err
	}

	logger.Debug("defining domain from xml")
	domain, err := conn.DomainDefineXML(domainXMLString)
	if err != nil {
		return fmt.Errorf("error defining domain from xml description: %v", err)
	}

	logger.Debug("booting domain")
	err = domain.Create()
	if err != nil {
		return fmt.Errorf("error creating domain: %v", err)
	}

	logger.Debug("domain booted")
	return nil
}
