// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

type AllNICs map[string]NICFilters

type NICFilters struct {
	Unfiltered bool     `json:",omitempty"`
	RouterPort string   `json:",omitempty"`
	IPs        []string `json:",omitempty"`
}
