// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"context"

	"github.com/sirupsen/logrus"

	"entanglement.garden/rhyzome/client/libvirtx"
)

func Start(ctx context.Context, id string) error {
	logger := logrus.WithField("instance", id)
	logger.Info("booting instance")

	conn, err := libvirtx.New()
	if err != nil {
		return err
	}
	defer conn.Close()

	logrus.Debug("finding domain")
	domain, err := conn.LookupDomainByName(IDToDomainName(id))
	if err != nil {
		return err
	}
	defer libvirtx.Free(domain)

	logrus.Debug("creating domain")
	return domain.Create()
}

func Stop(ctx context.Context, id string, graceful bool) error {
	logger := logrus.WithField("instance", id)
	logger.WithField("graceful", graceful).Info("stopping instance")

	conn, err := libvirtx.New()
	if err != nil {
		return err
	}
	defer conn.Close()

	logrus.Debug("finding domain")
	domain, err := conn.LookupDomainByName(IDToDomainName(id))
	if err != nil {
		return err
	}
	defer libvirtx.Free(domain)

	if graceful {
		logger.Debug("shutting down domain")
		return domain.Shutdown()
	} else {
		logger.Debug("destroying domain")
		return domain.Destroy()
	}
}
