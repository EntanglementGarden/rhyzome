// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"context"
	"fmt"

	"libvirt.org/go/libvirt"
	"libvirt.org/go/libvirtxml"

	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/client/config"
	"entanglement.garden/rhyzome/client/libvirtx"
	"entanglement.garden/rhyzome/protos"
)

func Ensure(ctx context.Context, instance *protos.Instance) error {
	ctx, log := logging.LogWithField(ctx, "instance", instance.Id)

	log.Debugf("ensuring instance is in state: %+v", instance)

	log.Debug("connecting to libvirt")
	conn, err := libvirtx.New()
	if err != nil {
		return fmt.Errorf("error connecting to libvirt: %v", err)
	}
	defer conn.Close()

	domain, err := conn.LookupDomainByName(IDToDomainName(instance.Id))
	if err != nil {
		if !libvirtx.IsErrorCode(err, libvirt.ERR_NO_DOMAIN) {
			return fmt.Errorf("error checking existing domain: %v", err)
		}
	} else {
		defer libvirtx.Free(domain)
	}

	if domain == nil {
		switch instance.State {
		// domain doesn't exist and is expected to
		case protos.Instance_PENDING, protos.Instance_CREATING, protos.Instance_STARTING, protos.Instance_RUNNING:
			log.Debug("vm for instance ", instance.Id, " doesn't exist, but instance state is ", instance.State.String(), ". creating VM")
			return create(conn, instance)
		case protos.Instance_DELETING, protos.Instance_DELETED:
			// instances doesn't exist and isn't expected to
			return nil
		default:
			return nil // instance doesn't exist and isn't expected to
		}
	}

	log.Debug("domain exists, ensuring state is correct")

	// ensure state matches expected
	err = ensureState(domain, instance)
	if err != nil {
		return err
	}

	if instance.State == protos.Instance_DELETED || instance.State == protos.Instance_DELETING {
		return nil
	}

	log.Debug("state is correct, ensuring networking is correctly configured")

	domainXML, err := getDomainXML(domain)
	if err != nil {
		return err
	}

	// ensure networks match. first loop over all expected networks and make sure it exists,
	// then loop over all network interfaces and drop any that weren't in the list
	expected := map[string]bool{}
	for _, nic := range instance.NetworkInterfaces {
		expected[nic.Mac] = true // record that a nic with this mac is expected to exist (everything not in the map is removed)

		found := false
		for _, i := range domainXML.Devices.Interfaces {
			if i.MAC.Address != nic.Mac {
				continue
			}

			// update the existing nic if it exists
			found = true
			err = ensureNic(ctx, domain, nic, &i)
			if err != nil {
				return err
			}

			break
		}

		if !found {
			// create a new nic if the requested nic wasn't attached on the vm
			err = ensureNic(ctx, domain, nic, nil)
			if err != nil {
				return err
			}
		}
	}

	for _, iface := range domainXML.Devices.Interfaces {
		if expected[iface.MAC.Address] {
			continue
		}

		ifaceXML, err := iface.Marshal()
		if err != nil {
			log.Error("error marshaling device XML")
			return err
		}

		log.Debug("removing interface ", ifaceXML)

		if err := domain.DetachDeviceFlags(ifaceXML, libvirt.DOMAIN_DEVICE_MODIFY_LIVE|libvirt.DOMAIN_DEVICE_MODIFY_CONFIG); err != nil {
			log.Error("error detaching interface from domain ", instance.Id)
			return err
		}
	}

	return nil
}

func ensureState(domain *libvirt.Domain, instance *protos.Instance) error {
	currentState, _, err := domain.GetState()
	if err != nil {
		return fmt.Errorf("error getting domain state: %v", err)
	}

	log := logging.GetLog(context.Background()).WithField("instance", instance.Id)

	switch instance.State {
	case protos.Instance_STOPPING: // stop has been requested
		if currentState == libvirt.DOMAIN_RUNNING { // stop was requested, instance is running, try to stop it
			log.Debug("stopping instance")
			if err := domain.DestroyFlags(libvirt.DOMAIN_DESTROY_GRACEFUL); err != nil {
				return err
			}
		}
	case protos.Instance_STARTING:
		if currentState != libvirt.DOMAIN_RUNNING {
			log.Debug("creating domain for instance")
			if err := domain.Create(); err != nil {
				return err
			}
		}
	case protos.Instance_DELETING, protos.Instance_DELETED: // deletion requested
		if currentState != libvirt.DOMAIN_SHUTOFF {
			log.Debug("destroying domain ", instance.Id)
			if err := domain.Destroy(); err != nil {
				return fmt.Errorf("error destorying domain: %v", err)
			}
		}

		log.Debug("undefining domain")
		if err := domain.Undefine(); err != nil {
			return fmt.Errorf("error undefining domain: %v", err)
		}
	case protos.Instance_REBOOTING:
		if currentState != libvirt.DOMAIN_RUNNING {
			log.Debug("not rebooting an instance that isnt running")
			break
		}

		log.Debug("rebooting instance")
		if err := domain.Reboot(libvirt.DOMAIN_REBOOT_ACPI_POWER_BTN); err != nil {
			log.Error("error rebooting domain: ", err)
		}

	}
	return nil
}

func ensureNic(ctx context.Context, domain *libvirt.Domain, requested *protos.NetworkInterface, actual *libvirtxml.DomainInterface) error {
	log := logging.GetLog(ctx).WithField("nic", requested.Mac)

	var final *libvirtxml.DomainInterface
	if actual != nil {
		if len(requested.Vlan) == 0 && actual.VLan == nil {
			return nil
		} else if len(requested.Vlan) == len(actual.VLan.Tags) {
			// TODO: better comparison here
			log.Debug("not updating vlan tags because lists are same length. requested: ", requested.Vlan, " actual: ", actual.VLan.Tags)
			return nil
		}
		final = actual
		log.Debug("updating nic because requested does not match actual")
	} else {
		log.Debugf("creating new nic: %+v", requested)
		final = &libvirtxml.DomainInterface{
			Source: &libvirtxml.DomainInterfaceSource{
				Bridge: &libvirtxml.DomainInterfaceSourceBridge{Bridge: config.C.BridgeInterface},
			},
			VirtualPort: &libvirtxml.DomainInterfaceVirtualPort{
				Params: &libvirtxml.DomainInterfaceVirtualPortParams{
					OpenVSwitch: &libvirtxml.DomainInterfaceVirtualPortParamsOpenVSwitch{},
				},
			},
			Model: &libvirtxml.DomainInterfaceModel{Type: "virtio"},
			MAC:   &libvirtxml.DomainInterfaceMAC{Address: requested.Mac},
		}
	}

	final.VLan = generateXMLVlan(requested)

	ifaceString, err := final.Marshal()
	if err != nil {
		return err
	}

	if actual != nil {
		err = domain.UpdateDeviceFlags(ifaceString, libvirt.DOMAIN_DEVICE_MODIFY_LIVE|libvirt.DOMAIN_DEVICE_MODIFY_CONFIG)
		if err != nil {
			return fmt.Errorf("error updating nic: %v", err)
		}
	} else {
		err = domain.AttachDeviceFlags(ifaceString, libvirt.DOMAIN_DEVICE_MODIFY_LIVE|libvirt.DOMAIN_DEVICE_MODIFY_CONFIG)
		if err != nil {
			return fmt.Errorf("error adding nic: %v", err)
		}
	}

	return nil
}

func generateXMLVlan(nic *protos.NetworkInterface) (vlan *libvirtxml.DomainInterfaceVLan) {
	vlan = &libvirtxml.DomainInterfaceVLan{}
	vlan.Trunk = ""
	nativeMode := "untagged"
	if nic.Tagged {
		vlan.Trunk = "yes"
		nativeMode = ""
	}

	for _, v := range nic.Vlan {
		vlan.Tags = append(vlan.Tags, libvirtxml.DomainInterfaceVLanTag{ID: uint(v), NativeMode: nativeMode})
	}

	return
}
