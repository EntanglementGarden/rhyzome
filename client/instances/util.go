// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package instances

import (
	"fmt"
	"strings"

	"libvirt.org/go/libvirt"
	"libvirt.org/go/libvirtxml"
)

func IDToDomainName(resourceID string) string {
	return fmt.Sprintf("rhyzome-%s", resourceID)
}

func DomainNameToID(domainname string) string {
	if !strings.HasPrefix(domainname, "rhyzome-") {
		return "" // domains not created by rhyzome have a null ID
	}

	return domainname[8:]
}

func getDomainXML(domain *libvirt.Domain) (d libvirtxml.Domain, err error) {
	var doc string
	doc, err = domain.GetXMLDesc(0)
	if err != nil {
		return
	}

	err = d.Unmarshal(doc)

	return
}
