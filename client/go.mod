module entanglement.garden/rhyzome/client

go 1.19

replace entanglement.garden/rhyzome/protos => ../protos

replace entanglement.garden/rhyzome/common => ../common

require (
	entanglement.garden/common v0.0.0-20231208203011-089e7d4f2ba5
	entanglement.garden/rhyzome/common v0.0.0-00010101000000-000000000000
	entanglement.garden/rhyzome/protos v0.0.0-00010101000000-000000000000
	github.com/golang-jwt/jwt/v4 v4.5.0
	github.com/sirupsen/logrus v1.9.3
	github.com/txn2/txeh v1.5.3
	github.com/vishvananda/netlink v1.1.0
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8
	google.golang.org/grpc v1.57.0
	gopkg.in/yaml.v2 v2.4.0
	libvirt.org/go/libvirt v1.9004.0
	libvirt.org/go/libvirtxml v1.9004.0
)

require (
	github.com/fsnotify/fsnotify v1.7.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/uuid v1.3.1 // indirect
	github.com/labstack/echo/v4 v4.11.1 // indirect
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/ory/keto/proto v0.11.1-alpha.0 // indirect
	github.com/stretchr/testify v1.8.3 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	github.com/vishvananda/netns v0.0.0-20191106174202-0a2b9b5464df // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/net v0.12.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20230525234030-28d5490b6b19 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
