// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcclient

import (
	"entanglement.garden/rhyzome/client/volumes"
	"entanglement.garden/rhyzome/protos"
)

func ensureVolume(event *protos.Event) error {
	if event.Instance != nil && event.Instance.State == protos.Instance_PENDING {
		// if the request also specifies an instance, mark the instance as creating when we start making the volume
		go MustSetInstanceState(event.Instance.Id, protos.Instance_CREATING)
	}

	if event.Volume.State == protos.Volume_PENDING {
		go MustSetVolumeState(event.Volume.Id, protos.Volume_CREATING)
	}

	err := volumes.Ensure(event.Volume)
	if err != nil {
		if event.Instance != nil {
			// if volume creation fails and the the request also specifies an instance, mark the instance as failed as well
			go MustSetInstanceState(event.Instance.Id, protos.Instance_ERROR)
		}
		go MustSetVolumeState(event.Volume.Id, protos.Volume_ERROR)
		return err
	}

	if event.Volume.State == protos.Volume_PENDING || event.Volume.State == protos.Volume_CREATING {
		if event.Instance != nil && event.Instance.RootVolume == event.Volume.Id {
			go MustSetVolumeState(event.Volume.Id, protos.Volume_ATTACHED)
		} else {
			go MustSetVolumeState(event.Volume.Id, protos.Volume_UNATTACHED)
		}
	}

	return nil
}
