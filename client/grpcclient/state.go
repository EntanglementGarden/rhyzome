// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcclient

import (
	"context"

	"github.com/sirupsen/logrus"

	"entanglement.garden/rhyzome/protos"
)

func SetInstanceState(instanceID string, state protos.Instance_State) error {
	if client == nil {
		return nil
	}

	_, err := client.InstanceStateChange(context.Background(), &protos.Instance{
		Id:    instanceID,
		State: state,
	})
	return err
}

func MustSetInstanceState(instanceID string, state protos.Instance_State) {
	err := SetInstanceState(instanceID, state)
	if err != nil {
		logrus.Error("error setting instance ", instanceID, " state to ", state.String(), ": ", err)
	}
}

func SetVolumeState(volumeID string, state protos.Volume_State) error {
	_, err := client.VolumeStateChange(context.Background(), &protos.Volume{
		Id:    volumeID,
		State: state,
	})
	return err
}

func MustSetVolumeState(volumeID string, state protos.Volume_State) {
	err := SetVolumeState(volumeID, state)
	if err != nil {
		logrus.Error("error setting volume ", volumeID, " state to ", state.String(), ": ", err)
	}
}
