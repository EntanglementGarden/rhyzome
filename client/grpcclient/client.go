// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcclient

import (
	"context"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"

	"entanglement.garden/common/retry"
	"entanglement.garden/rhyzome/client/config"
	"entanglement.garden/rhyzome/protos"
)

const maxBackoff = time.Minute // max backoff: 1 minute

var (
	conn              *grpc.ClientConn
	client            protos.RhyzomeClient
	eventStreamClient protos.Rhyzome_EventStreamClient
	shutdown          = false
	backoff           = time.Second
	imports           []*protos.Event
)

func ConnectAndProcessEvents() {
	retryer := retry.New()

	for {
		if shutdown {
			logrus.Debug("shutdown = true, not re-connecting")
			return
		}

		if conn == nil || conn.GetState() == connectivity.Shutdown {
			if err := connect(); err != nil {
				retryer.Retry(context.Background(), err)
				continue
			}
		}

		if conn.GetState() == connectivity.Ready {
			processIncomingEvents()
			conn.Close()
		} else {
			retryer.Retry(context.Background(), fmt.Errorf("connection is not ready: %s", conn.GetState().String()))
			continue
		}
	}
}

func ConnectOneShot() error {
	return connect()
}

func connect() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	logrus.Info("establishing grpc connection to ", config.C.APIServer)

	tlsConfig := config.C.PKI.GetClientCreds(config.C.APIServer)

	var err error
	conn, err = grpc.DialContext(ctx, config.C.APIServer, grpc.WithTransportCredentials(tlsConfig))
	if err != nil {
		logrus.Error("error dialing API server: ", err)
		return err
	}

	client = protos.NewRhyzomeClient(conn)

	logrus.Debug("connected to gRPC service")

	for len(imports) > 0 {
		i := len(imports) - 1

		logrus.Debugf("importing instance: %+v", imports[i])

		_, err := client.Import(ctx, imports[i])
		if err != nil {
			logrus.Error("error sending instance for import: ", imports[0])
			return err
		}

		if i > 0 {
			imports = imports[:i]
		} else {
			imports = []*protos.Event{}
		}
	}

	logrus.Info("connecting to event stream")
	eventStreamClient, err = client.EventStream(context.Background())
	if err != nil {
		logrus.Error("error getting event stream from rhyzome client: ", err)
		return err
	}

	return nil
}

func Shutdown() error {
	shutdown = true
	if conn == nil {
		return nil
	}
	logrus.Info("shutting down grpc client")
	return conn.Close()
}

func increaseBackoff() {
	backoff = backoff * 2
	if backoff > maxBackoff {
		backoff = maxBackoff
	}
}

func Import(event *protos.Event) {
	imports = append(imports, event)
}
