// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcclient

import (
	"context"
	"io"
	"time"

	"github.com/sirupsen/logrus"

	"entanglement.garden/rhyzome/client/instances"
	"entanglement.garden/rhyzome/protos"
)

func processIncomingEvents() {
	ctx := context.Background()

	for {
		if shutdown || eventStreamClient == nil {
			return
		}

		logrus.Info("asking for incoming events")
		event, err := eventStreamClient.Recv()
		if err != nil {
			if err == io.EOF {
				logrus.Info("Got EOF from GRPC server")
			} else {
				logrus.Error("error receiving event from grpc stream: ", err)
			}
			increaseBackoff()
			return
		}

		logrus.Debug("processing request from gRPC server")

		if event.Volume != nil {
			err = ensureVolume(event)
			if err != nil {
				logrus.Error("error ensuring volume state: ", err)
				go MustSetVolumeState(event.Volume.Id, protos.Volume_ERROR)
				if event.Instance != nil {
					go MustSetInstanceState(event.Instance.Id, protos.Instance_ERROR)
				}
				continue
			}
		}

		if event.Instance != nil {
			if event.Instance.State == protos.Instance_PENDING {
				go MustSetInstanceState(event.Instance.Id, protos.Instance_CREATING)
			}

			err = instances.Ensure(ctx, event.Instance)
			if err != nil {
				go MustSetInstanceState(event.Instance.Id, protos.Instance_ERROR)
				logrus.Error("error handling request: ", err)
				return
			} else if event.Instance.State == protos.Instance_DELETING {
				// if ensure worked and the instance is in state deleting, set it to deleted
				go MustSetInstanceState(event.Instance.Id, protos.Instance_DELETED)
			} else if event.Instance.State == protos.Instance_REBOOTING {
				go MustSetInstanceState(event.Instance.Id, protos.Instance_RUNNING)
			}

			if event.BootstrapComplete {
				err := bootstrapCleanup(event.Instance)
				if err != nil {
					logrus.Error("error cleaning up bootstrap: ", err)
				}
			}

		}

		logrus.Debug("successfully processed request")
		backoff = time.Second
	}
}
