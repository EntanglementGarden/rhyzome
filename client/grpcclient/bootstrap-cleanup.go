// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcclient

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/txn2/txeh"
	"github.com/vishvananda/netlink"
	"libvirt.org/go/libvirt"

	"entanglement.garden/rhyzome/client/config"
	"entanglement.garden/rhyzome/client/libvirtx"
	"entanglement.garden/rhyzome/common"
	"entanglement.garden/rhyzome/protos"
)

const SystemNetworkVlan = 10

func bootstrapCleanup(instance *protos.Instance) error {
	logrus.Debug("bootstrap complete message received")

	conn, err := libvirtx.New()
	if err != nil {
		return fmt.Errorf("unable to connect to libvirt: %v", err)
	}
	defer conn.Close()

	network, err := conn.LookupNetworkByName(common.NetworkIDBootstrap)
	if err != nil {
		if !libvirtx.IsErrorCode(err, libvirt.ERR_NO_NETWORK) {
			return fmt.Errorf("error getting bootstrap network: %v", err)
		}
		logrus.Debug("bootstrap network doesnt exist")
	} else {
		defer libvirtx.Free(network)
	}

	logrus.Debug("destroying libvirt bootstrap network")
	err = network.Destroy()
	if err != nil {
		return fmt.Errorf("error deleting bootstrap network: %v", err)
	}

	logrus.Debug("updating /etc/hosts")
	for _, nic := range instance.NetworkInterfaces {
		if nic.Bootstrap {
			continue
		}

		if nic.Ip == "" {
			logrus.Warn("refusing to set /etc/hosts to a blank IP. why does this NIC have a blank IP!? ", nic)
			continue
		}

		logrus.Debug("services vm instance received:", nic)
		err = SetGRPCHost(nic.Ip)
		if err != nil {
			return err
		}
		break
	}

	logrus.Debug("deleting networkd config for ip on wan bridge")
	if err := os.Remove("/etc/systemd/network/25-brwan.network"); err != nil {
		return err
	}

	logrus.WithField("bridge", config.C.WANBridge).Debug("dropping ip from wan bridge")
	br, err := netlink.LinkByName(config.C.WANBridge)
	if err != nil {
		return err
	}
	addrs, err := netlink.AddrList(br, 0)
	if err != nil {
		return err
	}
	for _, addr := range addrs {
		logrus.WithField("addr", addr.String()).Debug("dropping address from interface")
		if err := netlink.AddrDel(br, &addr); err != nil {
			return err
		}
	}

	logrus.Debug("done cleaning up from bootstrap")
	return nil
}

func SetGRPCHost(ip string) error {
	hosts, err := txeh.NewHosts(&txeh.HostsConfig{
		ReadFilePath: "/etc/hosts",
	})
	if err != nil {
		return err
	}

	hosts.RemoveHosts(config.GetFakeHosts()) // is this needed? unclear what AddHosts will do if the name already matches
	hosts.AddHosts(ip, config.GetFakeHosts())

	logrus.Info("editing /etc/hosts for ", ip, " -> ", config.GetFakeHosts())
	if err := hosts.Save(); err != nil {
		return err
	}

	return nil
}
