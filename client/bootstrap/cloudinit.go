// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"encoding/json"
	"fmt"

	"gopkg.in/yaml.v2"

	"entanglement.garden/common/cloudinit"
	"entanglement.garden/common/installdata"
	"entanglement.garden/rhyzome/client/config"
)

var ClusterBootstrapBin = "https://codeberg.org/api/packages/EntanglementGarden/generic/extensions/a9735a3/entanglement-garden-cluster-bootstrap"

func getCloudConfig(installData installdata.InstallData) ([]byte, error) {
	marshalledInstallData, err := json.Marshal(installData)
	if err != nil {
		return nil, fmt.Errorf("error marshaling install data: %v", err)
	}

	cloudConfig := cloudinit.UserData{
		Packages: []string{"tmux", "mosh", "htop", "kubernetes-client", "jq"}, // utilities for admins, none are required
		RunCmd: [][]string{
			{"networkctl", "reload"},
			{"bash", "-c", "curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC=\"server --disable-helm-controller --disable=traefik\" sh -"},
			{"sudo", "-u", "debian", "mkdir", "/home/debian/.kube"},
			{"cp", "/etc/rancher/k3s/k3s.yaml", "/home/debian/.kube/config"},
			{"chown", "debian:debian", "/home/debian/.kube/config"},
			{"wget", "--quiet", "-O", "/tmp/step-cli.deb", "https://dl.step.sm/gh-release/cli/gh-release-header/v0.19.0/step-cli_0.19.0_amd64.deb"},
			{"apt-get", "install", "-y", "/tmp/step-cli.deb"},
			{"wget", "--quiet", "-O", "/usr/sbin/entanglement-garden-cluster-bootstrap", ClusterBootstrapBin},
			{"chmod", "+x", "/usr/sbin/entanglement-garden-cluster-bootstrap"},
			{"/usr/sbin/entanglement-garden-cluster-bootstrap", config.C.Domain},
		},
		Hostname:          "entanglement-services",
		SSHAuthorizedKeys: installData.SSHAuthorizedKeys,
		WriteFiles: []cloudinit.File{
			{
				Path:        "/etc/containers/registries.conf.d/registry.conf",
				Content:     fmt.Sprintf("unqualified-search-registries = [\"%s\"]\n", installData.ContainerRegistry),
				Permissions: "0644",
			},
			{
				Path:        "/etc/systemd/network/25-system.network",
				Content:     fmt.Sprintf("[Match]\nMACAddress=%s\n\n[Network]\nDHCP=yes", installData.SystemInstanceMAC),
				Permissions: "0644",
			},
			{
				Path:        installdata.InstallDataPath,
				Content:     string(marshalledInstallData),
				Permissions: "0644",
			},
		},
		Apt: cloudinit.Apt{Primary: installData.AptMirrors},
	}

	userData, err := yaml.Marshal(cloudConfig)
	if err != nil {
		return []byte{}, fmt.Errorf("error preparing service instance user data: %v", err)
	}

	return append([]byte("#cloud-config\n"), userData...), nil
}
