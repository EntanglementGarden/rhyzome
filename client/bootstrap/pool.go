// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"libvirt.org/go/libvirt"
	"libvirt.org/go/libvirtxml"

	"github.com/sirupsen/logrus"

	"entanglement.garden/rhyzome/client/config"
	"entanglement.garden/rhyzome/client/libvirtx"
)

func ensurePool(conn *libvirt.Connect) error {
	s, err := conn.LookupStoragePoolByName(config.C.DiskStoragePool)
	if err == nil {
		libvirtx.Free(s)
		return nil // no error, pool exists
	}

	libvirtErr, ok := err.(libvirt.Error)
	if !ok || !libvirtErr.Is(libvirt.ERR_NO_STORAGE_POOL) {
		return err // some error other than the one we're expecting
	}

	pool := libvirtxml.StoragePool{
		Type: "dir",
		Name: config.C.DiskStoragePool,
		Target: &libvirtxml.StoragePoolTarget{
			Path: config.C.ImageDir,
			Permissions: &libvirtxml.StoragePoolTargetPermissions{
				Mode: "0755",
			},
		},
	}

	poolXML, err := pool.Marshal()
	if err != nil {
		return err
	}

	logrus.Debug("storage pool does not exist, creating")

	poolConn, err := conn.StoragePoolDefineXML(poolXML, 0)
	if err != nil {
		return err
	}
	defer libvirtx.Free(poolConn)

	err = poolConn.Create(libvirt.STORAGE_POOL_CREATE_NORMAL)
	if err != nil {
		return err
	}

	return nil
}
