// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"net/http"
	"strings"
	"sync"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	"entanglement.garden/common/cloudinit"
	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/client/grpcclient"
)

var (
	metadataPort   = 8080
	metadataServer http.Server
)

func httpListener(instanceID string, userdata []byte, addr string, wg *sync.WaitGroup) {
	wg.Add(1) // wait for POST with certificate issuance token to /ca-available

	mux := http.NewServeMux()

	mux.HandleFunc("/user-data", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write(userdata)
		if err != nil {
			logrus.Errorf("[%s] failed to response to HTTP request: %v", r.URL.Path, err)
		}
	})

	mux.HandleFunc("/meta-data", func(w http.ResponseWriter, r *http.Request) {
		err := yaml.NewEncoder(w).Encode(cloudinit.MetaData{InstanceID: instanceID})
		if err != nil {
			logrus.Errorf("[%s] failed to response to HTTP request: %v", r.URL.Path, err)
		}
	})

	mux.HandleFunc("/vendor-data", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNoContent)
	})

	mux.HandleFunc("/ca-available", func(w http.ResponseWriter, r *http.Request) {
		log := logging.GetLog(r.Context())

		fingerprint := r.PostFormValue("fingerprint")
		token := r.PostFormValue("token")
		remoteIP := strings.SplitN(r.RemoteAddr, ":", 2)[0]

		if err := grpcclient.SetGRPCHost(remoteIP); err != nil {
			log.Error("error editing /etc/hosts: ", err)
		}

		go func() {
			for {
				err := join(token, fingerprint)
				if err != nil {
					log.Error("error joining cluster, retrying: ", err)
					continue
				}
				break
			}
			wg.Done()
		}()
	})

	metadataServer = http.Server{
		Addr:    addr,
		Handler: httpLogger{next: mux},
	}

	logrus.Debug("starting metadata http server on ", addr)
	_ = metadataServer.ListenAndServe()
}

type httpLogger struct {
	next http.Handler
}

func (h httpLogger) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx, log := logging.LogWithField(r.Context(), "path", r.URL)
	h.next.ServeHTTP(w, r.WithContext(ctx))
	log.Info("request completed")
}
