// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"

	"entanglement.garden/common/cluster"
	"entanglement.garden/rhyzome/client/config"
)

func join(token, caFingerprint string) error {
	if err := RunStepCommand("ca", "bootstrap", "--force", "--ca-url", fmt.Sprintf("https://ca.%s:8443", cluster.Domain), "--fingerprint", caFingerprint); err != nil {
		return fmt.Errorf("error initializing step locally: %v", err)
	}

	subject, err := GetJWTSubject(token)
	if err != nil {
		return fmt.Errorf("error parsing join token: %v", err)
	}

	certPath := filepath.Join(stepPath, "rhyzome-client.crt")
	keyPath := filepath.Join(stepPath, "rhyzome-client.key")

	if err := RunStepCommand("ca", "certificate", "--force", "--token", token, subject, certPath, keyPath); err != nil {
		return fmt.Errorf("error getting certificate: %v", err)
	}
	logrus.Info("local certificates configured in ", filepath.Join(stepPath, fmt.Sprintf("%s.crt", subject)))

	config.C.APIServer = fmt.Sprintf("entanglement-garden-rhyzome.%s:9090", cluster.Domain)
	config.C.PKI.Cert = certPath
	config.C.PKI.Key = keyPath
	config.C.PKI.CA = caPath

	f, err := os.Create("/etc/entanglement.garden/rhyzome.json")
	if err != nil {
		return fmt.Errorf("error creating /etc/entanglement.garden/rhyzome.json: %v", err)
	}
	defer f.Close()

	err = json.NewEncoder(f).Encode(&config.C)
	if err != nil {
		return fmt.Errorf("error encoding new config: %v", err)
	}

	logrus.Info("Wrote config to /etc/entanglement.garden/rhyzome.json")

	return nil
}
