// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"libvirt.org/go/libvirt"
	"libvirt.org/go/libvirtxml"

	"entanglement.garden/rhyzome/client/config"
	"entanglement.garden/rhyzome/client/libvirtx"
	"entanglement.garden/rhyzome/common"
)

var (
	bootstrapNetworkLocalIP = "192.168.100.1" // be sure to update DHCP range below if any of the first three stanzas change
)

func getBindIP(conn *libvirt.Connect) (string, error) {
	net, err := conn.LookupNetworkByName(common.NetworkIDBootstrap)
	if err != nil {
		return "", err
	}
	defer libvirtx.Free(net)

	netXMLStr, err := net.GetXMLDesc(0)
	if err != nil {
		return "", err
	}

	netXML := libvirtxml.Network{}
	err = netXML.Unmarshal(netXMLStr)
	if err != nil {
		return "", err
	}

	return netXML.IPs[0].Address, nil
}

func ensureLibvirtNetwork(conn *libvirt.Connect) error {
	n, err := conn.LookupNetworkByName(common.NetworkIDBootstrap)
	if err == nil {
		libvirtx.Free(n)
		return nil // network already exists
	}

	network := libvirtxml.Network{
		Name:    common.NetworkIDBootstrap,
		Forward: &libvirtxml.NetworkForward{Mode: "nat"},
		Domain:  &libvirtxml.NetworkDomain{Name: config.C.Domain},
		IPs: []libvirtxml.NetworkIP{
			{
				Address: bootstrapNetworkLocalIP,
				DHCP: &libvirtxml.NetworkDHCP{
					Ranges: []libvirtxml.NetworkDHCPRange{
						{Start: "192.168.100.128", End: "192.168.100.254"},
					},
				},
			},
		},
	}
	networkXML, err := network.Marshal()
	if err != nil {
		return err
	}

	_, err = conn.NetworkCreateXML(networkXML)
	if err != nil {
		return err
	}

	return nil
}
