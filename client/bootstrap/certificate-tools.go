// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	jwt "github.com/golang-jwt/jwt/v4"
	"github.com/sirupsen/logrus"

	"entanglement.garden/rhyzome/common"
)

var (
	jwtParser = jwt.Parser{}
	stepPath  = filepath.Join("etc", "entanglement.garden", "step")
	caPath    = filepath.Join(stepPath, "certs", "root_ca.crt")
)

// RunStepCommand shells out to step and logs any stdout/stderr output, returning an error if the command exits non-zero
func RunStepCommand(args ...string) error {
	if _, err := os.Stat(stepPath); os.IsNotExist(err) {
		err = os.MkdirAll(stepPath, 0750)
		if err != nil {
			return err
		}
	}
	logrus.Debug("executing step: ", args)

	infoLogger := logrus.StandardLogger().WriterLevel(logrus.InfoLevel)
	defer infoLogger.Close()
	warnLogger := logrus.StandardLogger().WriterLevel(logrus.WarnLevel)
	defer warnLogger.Close()

	bootstrapCmd := exec.Command("step", args...)
	bootstrapCmd.Env = append(bootstrapCmd.Env, fmt.Sprintf("STEPPATH=%s", stepPath))
	bootstrapCmd.Stdout = infoLogger
	bootstrapCmd.Stderr = warnLogger
	if err := bootstrapCmd.Run(); err != nil {
		return err
	}
	return nil
}

// GetProvisioningInfo fetches provisioning information from rhyzome-apiserver
func GetProvisioningInfo(vmIP, caFingerprint string) (p common.ProvisioningInfo, err error) {
	baseURL := "https://" + vmIP

	client, ca, err := getClient(baseURL, caFingerprint)
	if err != nil {
		return p, err
	}

	f, err := os.Create(caPath)
	if err != nil {
		return p, err
	}
	defer f.Close()

	if _, err := f.Write(ca); err != nil {
		logrus.Error("error writing to CA file ", caPath)
		return p, err
	}

	url := baseURL + "/.well-known/rhyzome/provisioning.json"
	logrus.Debug("requesting ", url)
	resp, err := client.Get(url)
	if err != nil {
		return p, err
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&p)
	if err != nil {
		return p, err
	}

	return p, nil
}

// getClient downloads and validates the CA root certificate, then returns an http client that trusts it
func getClient(baseURL, caFingerprint string) (*http.Client, []byte, error) {
	url := baseURL + "/.well-known/rhyzome/ca.pem"

	// insecureClient is an https client that doesn't validate certificates
	insecureClient := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	logrus.Debug("requesting ", url)
	resp, err := insecureClient.Get(url)
	if err != nil {
		return nil, nil, err
	}
	defer resp.Body.Close()

	caPem, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, nil, err
	}

	block, rest := pem.Decode(caPem)
	if block == nil {
		logrus.Debug("rest of ca cert: ", string(rest))
		return nil, nil, errors.New("error decoding certificate")
	}

	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, nil, err
	}

	sum := sha256.Sum256(cert.Raw)
	actualFingerprint := strings.ToLower(hex.EncodeToString(sum[:]))
	if !strings.EqualFold(caFingerprint, actualFingerprint) {
		return nil, nil, fmt.Errorf("ca did not match expected fingerprint (expected %s got %s)", caFingerprint, actualFingerprint)
	}

	pool := x509.NewCertPool()
	pool.AppendCertsFromPEM(caPem)

	return &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: pool,
			},
		},
	}, caPem, nil
}

// GetJWTSubject parses the `sub` property out of a JWT
func GetJWTSubject(tokenString string) (string, error) {
	var claims jwt.MapClaims
	_, _, err := jwtParser.ParseUnverified(tokenString, &claims)
	if err != nil {
		return "", err
	}

	return claims["sub"].(string), nil
}
