// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"context"
	"fmt"
	"net"
	"os"
	"strings"
	"sync"

	"entanglement.garden/common/identifiers"
	"entanglement.garden/common/installdata"
	"entanglement.garden/rhyzome/client/config"
	"entanglement.garden/rhyzome/client/grpcclient"
	"entanglement.garden/rhyzome/client/instances"
	"entanglement.garden/rhyzome/client/libvirtx"
	"entanglement.garden/rhyzome/client/volumes"
	"entanglement.garden/rhyzome/common"
	"entanglement.garden/rhyzome/protos"
)

func Run() error {
	installData, err := installdata.ReadInstallData()
	if err != nil {
		return err
	}
	config.C.Domain = installData.ClusterDomain

	nodeID, err := os.ReadFile("/etc/hostname")
	if err != nil {
		return err
	}
	installData.FirstNodeID = strings.TrimSpace(string(nodeID))

	conn, err := libvirtx.New()
	if err != nil {
		return fmt.Errorf("unable to connect to libvirt: %v", err)
	}
	defer conn.Close()

	hypervisor, err := getHypervisor()
	if err != nil {
		return err
	}

	installData.FirstNodeMAC = hypervisor.NetworkInterfaces[0].Mac
	installData.FirstNodeNIC = identifiers.GenerateID(identifiers.PrefixInstanceNic)

	// ensure the disk pool exists
	err = ensurePool(conn)
	if err != nil {
		return fmt.Errorf("error ensuring pool: %v", err)
	}

	// create bootstrap network
	if err := ensureLibvirtNetwork(conn); err != nil {
		return fmt.Errorf("error creating network: %v", err)
	}

	bindIP, err := getBindIP(conn)
	if err != nil {
		return fmt.Errorf("error getting bridge ip: %v", err)
	}
	addr := fmt.Sprintf("%s:%d", bindIP, metadataPort)
	installData.HypervisorCallbackURL = fmt.Sprintf("http://%s", addr)

	installData.SystemInstanceID = identifiers.GenerateID(identifiers.PrefixInstance)
	installData.SystemInstanceNIC = identifiers.GenerateID(identifiers.PrefixInstanceNic)
	installData.SystemInstanceMAC, err = common.GenerateMAC(config.C.MACPrefix)
	if err != nil {
		return err
	}

	vol := protos.Volume{
		Id:              identifiers.GenerateID(identifiers.PrefixVolume),
		Size:            25,
		SourceImageQcow: installData.VMBaseImageQcow,
		State:           protos.Volume_PENDING,
	}

	installData.SystemInstanceBootstrapMAC, err = common.GenerateMAC(config.C.MACPrefix)
	if err != nil {
		return err
	}

	instance := &protos.Instance{
		Id:         installData.SystemInstanceID,
		State:      protos.Instance_PENDING,
		RootVolume: vol.Id,
		Memory:     4096, // might be able to reduce this, but at 1024 it's causing issues
		Cores:      2,
		NetworkInterfaces: []*protos.NetworkInterface{
			{
				Id:        identifiers.GenerateID(identifiers.PrefixInstanceNic),
				Mac:       installData.SystemInstanceBootstrapMAC,
				Bootstrap: true,
			},
		},
		Biosdata: fmt.Sprintf("ds=nocloud-net;s=%s/", installData.HypervisorCallbackURL),
	}

	if err := volumes.Ensure(&vol); err != nil {
		return err
	}

	userData, err := getCloudConfig(installData)
	if err != nil {
		return err
	}

	var wg sync.WaitGroup

	// start metadata server
	go httpListener(installData.SystemInstanceMAC, userData, addr, &wg)
	defer func() {
		metadataServer.Close()
		// hosts, err := cleanEtcHosts()
		// if err != nil {
		// 	log.Error("error fixing /etc/hosts: ", err)
		// } else if hosts != nil {
		// 	_ = hosts.Save()
		// }
	}()

	if err := instances.Ensure(context.Background(), instance); err != nil {
		return err
	}

	instance.State = protos.Instance_RUNNING

	grpcclient.Import(&protos.Event{
		Instance: instance,
		Volume:   &vol,
	})

	wg.Wait()

	return nil
}

func getHypervisor() (*protos.Instance, error) {
	// get the eg0 mac address
	systemIface, err := net.InterfaceByName("eg0")
	if err != nil {
		return nil, fmt.Errorf("error getting MAC of eg0: %v", err)
	}

	hostname, err := os.Hostname()
	if err != nil {
		return nil, fmt.Errorf("error getting hostname: %v", err)
	}

	return &protos.Instance{
		Id: hostname,
		NetworkInterfaces: []*protos.NetworkInterface{
			{
				Id:  identifiers.GenerateID(identifiers.PrefixInstanceNic),
				Mac: systemIface.HardwareAddr.String(),
			},
		},
	}, nil
}
