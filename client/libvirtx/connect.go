// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package libvirtx

import (
	"github.com/sirupsen/logrus"
	"libvirt.org/go/libvirt"
)

func New() (*libvirt.Connect, error) {
	logrus.Debug("connecting to libvirt")
	return libvirt.NewConnect("qemu:///system")
}
