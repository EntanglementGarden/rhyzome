// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package libvirtx

import (
	"libvirt.org/go/libvirt"
)

type Stream struct {
	*libvirt.Stream
}

func (s Stream) Read(buf []byte) (int, error) {
	return s.Stream.Recv(buf)
}

func (s Stream) Close() error {
	return s.Stream.Free()
}
