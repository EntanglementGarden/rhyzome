// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package libvirtx

import (
	"github.com/sirupsen/logrus"
	"libvirt.org/go/libvirt"
)

func IsErrorCode(err error, number libvirt.ErrorNumber) bool {
	if err == nil {
		return false
	}

	lerr, ok := err.(libvirt.Error)
	if !ok {
		return false
	}

	return lerr.Code == number
}

func Free(freeable interface{ Free() error }) {
	err := freeable.Free()
	if err != nil {
		logrus.Error("uncaught error freeing libvirt resource: ", err)
	}
}
