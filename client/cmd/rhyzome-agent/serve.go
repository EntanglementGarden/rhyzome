// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/writer"

	"entanglement.garden/rhyzome/client/bootstrap"
	"entanglement.garden/rhyzome/client/config"
	"entanglement.garden/rhyzome/client/grpcclient"
	"entanglement.garden/rhyzome/client/hookipc"
)

var signals = make(chan os.Signal, 1)

func serve() {
	config.Load()

	for _, tty := range []string{"/dev/ttyS0", "/dev/tty1"} {
		serialLogger, err := os.Create(tty)
		if err != nil {
			logrus.Warn("failed to open ", tty, " for log writing: ", err)
		} else {
			defer serialLogger.Close()

			logrus.AddHook(&writer.Hook{
				Writer: serialLogger,
				LogLevels: []logrus.Level{
					logrus.PanicLevel,
					logrus.FatalLevel,
					logrus.ErrorLevel,
					logrus.WarnLevel,
				},
			})
		}
	}

	go hookipc.ListenAndServe()

	for config.C.APIServer == "" {
		err := bootstrap.Run()
		if err != nil {
			logrus.Error("error bootstrapping: ", err)
			logrus.Info("retrying bootstrap in 1 minute")
			time.Sleep(time.Minute)
		} else {
			logrus.Info("cluster bootstrapping has run successfully. We will now start the gRPC client")
			break
		}
	}

	logrus.Info("connecting")
	go grpcclient.ConnectAndProcessEvents()

	signal.Notify(signals, syscall.SIGINT, syscall.SIGHUP)
	for {
		signal := <-signals
		logrus.Debug("received signal", signal)
		switch signal {
		case syscall.SIGHUP:
			config.Load()

		case syscall.SIGINT:
			logrus.Info("shutting down IPC listener")
			if err := hookipc.Shutdown(); err != nil {
				logrus.Error("error shutting down hookipc server: ", err)
			}

			logrus.Info("shutting down gRPC client")
			if err := grpcclient.Shutdown(); err != nil {
				logrus.Error("error shutting down grpc client: ", err)
			}

			return
		}
	}
}
