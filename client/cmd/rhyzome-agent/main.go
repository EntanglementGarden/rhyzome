// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"fmt"
	"os"

	"entanglement.garden/common/config"
)

func main() {
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "hook":
			hook(os.Args[2:]...)
			return
		case "--version":
			fmt.Println("rhyzome-libvirt version", config.Version)
			return
		}
	}

	serve()
}
