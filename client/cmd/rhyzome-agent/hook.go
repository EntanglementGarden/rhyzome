// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"log/syslog"
	"os"
	"os/exec"

	"github.com/sirupsen/logrus"
	lSyslog "github.com/sirupsen/logrus/hooks/syslog"
	"libvirt.org/go/libvirtxml"

	"entanglement.garden/rhyzome/client/config"
	"entanglement.garden/rhyzome/client/hookipc"
	"entanglement.garden/rhyzome/client/instances"
	"entanglement.garden/rhyzome/protos"
)

var syslogger = logrus.New()

func hook(args ...string) {
	syslog, err := lSyslog.NewSyslogHook("", "", syslog.LOG_INFO, "rhyzome-libvirt-hook")
	if err == nil {
		syslogger.Hooks.Add(syslog)
	}

	if len(args) != 4 {
		syslogger.Error("incorrect number of arguments")
		return
	}

	syslogger.SetLevel(logrus.DebugLevel)

	config.Load()

	guestName := args[0]
	switch args[1] { // operation
	case "start":
		err := prepareOVSPorts()
		if err != nil {
			syslogger.Error("error preparing OpenVSwitch ports for instance: ", err)
			return
		}
		syslogger.Debug("prepared NICs for instance")
	case "started":
		err = hookipc.InstanceStateChanged(guestName, protos.Instance_RUNNING)
		if err != nil {
			syslogger.Error("error submitting instance started event: ", err)
			return
		}
		syslogger.Debug("marked instance started")
	case "stopped":
		err := hookipc.InstanceStateChanged(guestName, protos.Instance_STOPPED)
		if err != nil {
			syslogger.Error("error submitting instance stopped event: ", err)
			return
		}
		syslogger.Debug("marked instance stopped")
	case "release":
		err := hookipc.InstanceStateChanged(guestName, protos.Instance_DELETED)
		if err != nil {
			syslogger.Error("error submitting instance deleted event: ", err)
			return
		}
		syslogger.Debug("marked instance deleted")
	default:
		syslogger.Debug("ignored operation ", args[1])
	}
}

func prepareOVSPorts() error {
	domainXML := libvirtxml.Domain{}
	if err := xml.NewDecoder(os.Stdin).Decode(&domainXML); err != nil {
		return fmt.Errorf("error parsing stdin: %v", err)
	}

	var firewallInfo instances.AllNICs
	if err := json.Unmarshal([]byte(domainXML.Description), &firewallInfo); err != nil {
		syslogger.Debug("invalid description: ", domainXML.Description)
		return fmt.Errorf("error parsing description as JSON: %v", err)
	}

	for _, iface := range domainXML.Devices.Interfaces {
		// skip WAN ports and ports that aren't connected to openvswitch
		if iface.Target == nil || iface.VirtualPort == nil {
			continue
		}

		debugxml, _ := iface.Marshal()
		syslogger.Debug("preparing port ", debugxml)

		info := firewallInfo[iface.MAC.Address]
		port := iface.Target.Dev

		flows := []string{}
		if info.Unfiltered {
			flows = append(flows, fmt.Sprintf("priority=100,in_port=%s,action=normal", port))
		} else {
			for _, ip := range info.IPs {
				flows = append(flows,
					fmt.Sprintf("priority=100,arp,in_port=%s,eth_src=%s,actions=normal", port, iface.MAC.Address),
					fmt.Sprintf("priority=100,ip,in_port=%s,dl_src=%s,nw_src=%s,actions=normal", port, iface.MAC.Address, ip),
					fmt.Sprintf("priority=100,ip,in_port=%s,eth_src=%s,ip_src=0.0.0.0,ip_dst=255.255.255.255,actions=normal", port, iface.MAC.Address),
				)
			}
		}

		for _, flow := range flows {
			stdin := bytes.NewBufferString(flow)
			syslogger.Debug("adding ovs flow: ", stdin.String())
			cmd := exec.Command("ovs-ofctl", "add-flow", config.C.BridgeInterface, "-")
			cmd.Stdin = stdin
			cmd.Stderr = syslogger.WriterLevel(logrus.WarnLevel)
			cmd.Stdout = syslogger.WriterLevel(logrus.DebugLevel)
			err := cmd.Run()
			if err != nil {
				return err
			}
		}
	}

	return nil
}
