// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package hookipc

import (
	"net"
	"net/rpc"
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"

	"entanglement.garden/rhyzome/client/config"
)

var listener net.Listener

func ListenAndServe() {
	rpcServer := rpc.NewServer()

	err := rpcServer.Register(new(Messages))
	if err != nil {
		logrus.Fatal("error registering rpc")
	}

	socketPath := getSocket()

	if err := os.Remove(socketPath); err != nil && !os.IsNotExist(err) {
		logrus.Fatal("error preparing local IPC socket: ", err)
	}

	listener, err = net.Listen("unix", socketPath)
	if err != nil {
		logrus.Fatal("error starting rpc listener on ", socketPath, ": ", err)
	}

	rpcServer.Accept(listener)
}

func Shutdown() error {
	if err := listener.Close(); err != nil {
		return err
	}

	return nil
}

func getSocket() string {
	return filepath.Join(config.C.SharedDir, "hook.sock")
}
