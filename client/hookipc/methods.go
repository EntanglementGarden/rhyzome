// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package hookipc

import (
	"github.com/sirupsen/logrus"

	"entanglement.garden/rhyzome/client/grpcclient"
	"entanglement.garden/rhyzome/client/instances"
	"entanglement.garden/rhyzome/protos"
)

type GuestState struct {
	GuestName string
	State     protos.Instance_State
}
type Messages struct{}

func (m *Messages) InstanceState(state *GuestState, reply *bool) error {
	instanceID := instances.DomainNameToID(state.GuestName)

	logrus.Debug("setting instance state: ", state)
	err := grpcclient.SetInstanceState(instanceID, state.State)
	if err != nil {
		logrus.Error("error sending state change to server: ", err)
		return err
	}

	return nil
}
