// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package hookipc

import (
	"fmt"
	"net/rpc"

	"entanglement.garden/rhyzome/protos"
)

func InstanceStateChanged(guestName string, state protos.Instance_State) error {
	socketPath := getSocket()

	client, err := rpc.Dial("unix", socketPath)
	if err != nil {
		return fmt.Errorf("error connecting to %s: %v", socketPath, err)
	}
	defer client.Close()

	var reply bool
	err = client.Call("Messages.InstanceState", &GuestState{GuestName: guestName, State: state}, &reply)
	if err != nil {
		return fmt.Errorf("error calling InstanceState RPC method: %v", err)
	}

	return nil
}
