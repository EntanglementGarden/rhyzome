// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package volumes

import (
	"fmt"
	"net/url"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"libvirt.org/go/libvirt"

	"entanglement.garden/rhyzome/client/libvirtx"
	"entanglement.garden/rhyzome/client/volumes/pools"
	"entanglement.garden/rhyzome/protos"
)

func Ensure(volume *protos.Volume) error {
	logger := logrus.WithField("volume", volume.Id)

	conn, err := libvirtx.New()
	if err != nil {
		return fmt.Errorf("error connecting to libvirt: %v", err)
	}
	defer conn.Close()

	pool, err := pools.GetPool(conn)
	if err != nil {
		return fmt.Errorf("error getting storage pool: %v", err)
	}
	defer libvirtx.Free(pool)

	if volume.State == protos.Volume_DELETED {
		logger.Debug("deleting volume from pool")
		err := pool.DeleteVolume(volume.Id)
		if err != nil && !libvirtx.IsErrorCode(err, libvirt.ERR_NO_STORAGE_VOL) {
			return err
		}
		return nil
	}

	vol, err := pool.LookupVolume(volume.Id)
	if err != nil {
		if !libvirtx.IsErrorCode(err, libvirt.ERR_NO_STORAGE_VOL) {
			return fmt.Errorf("unexpected error checking for volume %s: %v", volume.Id, err)
		}

		// volume doesn't exist, create it
		vol, err := pool.CreateVolume(volume.Id, uint64(volume.Size))
		if err != nil {
			return fmt.Errorf("error creating volume: %v", err)
		}
		defer libvirtx.Free(vol)

		sourceImage := ""
		switch pool.ImageFormat() {
		case pools.ImageFormatRaw:
			sourceImage = volume.SourceImage
		case pools.ImageFormatQcow2:
			sourceImage = volume.SourceImageQcow
		}

		if sourceImage != "" {
			logger.Debug("Downloading image ", sourceImage)
			start := time.Now()
			if strings.HasPrefix(sourceImage, "https://") || strings.HasPrefix(sourceImage, "http://") {
				var imageurl *url.URL
				imageurl, err = url.Parse(sourceImage)
				if err == nil {
					err = downloadToVolume(sourceImage, imageurl.Fragment, false, vol, conn, int(volume.Size))
				}
			} else {
				err = downloadImageFromLegacyStore(sourceImage, vol, conn, volume.Id, int(volume.Size))
			}
			if err != nil {
				_ = vol.Delete(0) // If we put a 1 it will overwrite all the data from the disk, see https://libvirt.org/html/libvirt-libvirt-storage.html#virStorageVolDeleteFlags
				return err
			}
			logger.Debug("downloaded image in ", time.Since(start))
		} else {
			logger.Debug("created empty disk (no source image provided)")
		}

		return nil
	} else {
		defer libvirtx.Free(vol)
	}

	info, err := vol.GetInfo()
	if err != nil {
		return fmt.Errorf("error getting info for %s: %v", volume.Id, err)
	}

	// why does libvirt seem to add 512 to my test volume? is this a qcow2-only thing?
	if info.Capacity != uint64(volume.Size)*1000*1000*1000+512 {
		logger.Debug("volume currently has ", info.Capacity, " bytes but requested size is ", uint64(volume.Size)*1000*1000*1000+512, " bytes")
		logger.Error("ensure does not currently support resizing volumes")
		return nil
	}

	return nil
}

func Delete(id string) error {
	logger := logrus.WithField("volume_id", id)
	logger.Info("deleting volume")

	conn, err := libvirtx.New()
	if err != nil {
		return err
	}
	defer conn.Close()

	pool, err := pools.GetPool(conn)
	if err != nil {
		return err
	}
	defer libvirtx.Free(pool)

	logger.Debug("requesting volume deletion")
	err = pool.DeleteVolume(pool.GetVolumeName(id))
	if err != nil {
		return err
	}

	logger.Debug("volume deleted")
	return nil
}
