// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package pools

import (
	"fmt"

	"libvirt.org/go/libvirt"
	"libvirt.org/go/libvirtxml"
)

const (
	StoragePoolTypeGeneric StoragePoolType = "generic"
	ImageFormatRaw         ImageFormat     = "raw"
)

// GenericPool is a StoragePool of most pool types other than dir (lvm, gluster, etc)
type GenericPool struct {
	pool *libvirt.StoragePool
	name string
}

func (p GenericPool) Type() StoragePoolType {
	return StoragePoolTypeGeneric
}

func (p GenericPool) ImageFormat() ImageFormat {
	return ImageFormatRaw
}

func (p GenericPool) CreateVolume(name string, sizeGB uint64) (*libvirt.StorageVol, error) {
	volumeXML := &libvirtxml.StorageVolume{
		Name: p.GetVolumeName(name),
		Capacity: &libvirtxml.StorageVolumeSize{
			Unit:  "GB",
			Value: sizeGB,
		},
	}

	xmlstr, err := volumeXML.Marshal()
	if err != nil {
		return nil, err
	}

	return p.pool.StorageVolCreateXML(xmlstr, 0)
}

func (p GenericPool) DeleteVolume(name string) error {
	vol, err := p.pool.LookupStorageVolByName(name)
	if err != nil {
		return err
	}
	return vol.Delete(libvirt.STORAGE_VOL_DELETE_NORMAL)
}

func (p GenericPool) GetVolumeName(name string) string {
	return fmt.Sprintf("rhyzome-%s", name)
}

func (p GenericPool) ResizeVolume(name string, newDiskSizeGB uint64) error {
	vol, err := p.pool.LookupStorageVolByName(name)
	if err != nil {
		return err
	}

	return vol.Resize(newDiskSizeGB*1024*1024*1024, 0)
}

func (p GenericPool) GetDomainDiskXML(name string) libvirtxml.DomainDisk {
	return libvirtxml.DomainDisk{
		Device: "disk",
		Driver: &libvirtxml.DomainDiskDriver{Name: "qemu", Type: "raw"},
		Source: &libvirtxml.DomainDiskSource{
			Volume: &libvirtxml.DomainDiskSourceVolume{Pool: p.name, Volume: p.GetVolumeName(name)},
		},
		Target: &libvirtxml.DomainDiskTarget{Dev: "vda"},
	}
}

func (p GenericPool) LookupVolume(name string) (*libvirt.StorageVol, error) {
	return p.pool.LookupStorageVolByName(p.GetVolumeName(name))
}

func (p GenericPool) Free() error {
	return p.pool.Free()
}

// NewGenericPool creates a wrapper for a generic pool
func NewGenericPool(pool *libvirt.StoragePool) (GenericPool, error) {
	name, err := pool.GetName()
	if err != nil {
		return GenericPool{}, err
	}

	return GenericPool{pool: pool, name: name}, nil
}
