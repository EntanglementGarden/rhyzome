// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package pools

import (
	"libvirt.org/go/libvirt"
	"libvirt.org/go/libvirtxml"

	"entanglement.garden/rhyzome/client/config"
)

type StoragePoolType string
type ImageFormat string

// StoragePool is an interface for the functionality around a type of libvirt.StoragePool
type StoragePool interface {
	CreateVolume(string, uint64) (*libvirt.StorageVol, error)
	DeleteVolume(string) error
	GetVolumeName(string) string
	ResizeVolume(string, uint64) error
	GetDomainDiskXML(string) libvirtxml.DomainDisk
	LookupVolume(string) (*libvirt.StorageVol, error)
	Type() StoragePoolType
	ImageFormat() ImageFormat
	Free() error
}

var drivers = map[string]func(*libvirt.StoragePool) (StoragePool, error){}

// GetPool retrieves the configured Storage Pool from libvirt
func GetPool(conn *libvirt.Connect) (StoragePool, error) {
	pool, err := conn.LookupStoragePoolByName(config.C.DiskStoragePool)
	if err != nil {
		return nil, err
	}

	xmldescription, err := pool.GetXMLDesc(0)
	if err != nil {
		return nil, err
	}

	p := libvirtxml.StoragePool{}
	err = p.Unmarshal(xmldescription)
	if err != nil {
		return nil, err
	}

	driver, ok := drivers[p.Type]
	if !ok {
		return NewGenericPool(pool)
	}

	return driver(pool)
}
