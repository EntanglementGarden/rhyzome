// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package volumes

import (
	"compress/bzip2"
	"compress/gzip"
	"context"
	"crypto/sha256"
	"crypto/sha512"
	"fmt"
	"hash"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/xi2/xz"
	"libvirt.org/go/libvirt"

	"entanglement.garden/common/httpx"
	"entanglement.garden/rhyzome/client/config"
	"entanglement.garden/rhyzome/client/libvirtx"
	"entanglement.garden/rhyzome/client/volumes/pools"
)

func downloadImageFromLegacyStore(imageName string, volume *libvirt.StorageVol, conn *libvirt.Connect, volumeID string, sizeGB int) error {
	hashes, err := getHashes(imageName)
	if err != nil {
		return err
	}

	filename := fmt.Sprintf("%s.qcow2", imageName)
	compressed := false
	if _, ok := hashes[filename]; !ok {
		filename = fmt.Sprintf("%s.xz", filename)
		compressed = true
	}

	url := fmt.Sprintf("%s/%s/%s", config.C.ImageHost, imageName, filename)
	return downloadToVolume(url, hashes[filename], compressed, volume, conn, sizeGB)
}

func downloadToVolume(imageURL string, expectedHash string, compressed bool, volume *libvirt.StorageVol, conn *libvirt.Connect, sizeGB int) error {
	var reader io.Reader
	var contentLength int64
	start := time.Now()

	parsedImageURL, err := url.Parse(imageURL)
	if err != nil {
		logrus.Error("error parsing URL")
		return err
	}

	// parse everything after the # in the URL as a query, so chain arguments with &
	fragmentQuery, err := url.ParseQuery(parsedImageURL.Fragment)
	if err != nil && parsedImageURL.Fragment != "" {
		return err
	}

	var downloadedImageHash hash.Hash

	// this is fully untested, but the idea is something like:
	// https://example.whatever/img.qcow2#hash=sha256:e8c7c3c983718ebc78d8738f562d55bfa77c4cf6f08241d246861d5ea9eb9cd2
	// sha256 and sha512 supported
	fragmenthash := strings.SplitN(fragmentQuery.Get("hash"), ":", 2)
	if len(fragmenthash) == 2 {
		switch fragmenthash[0] {
		case "sha256":
			downloadedImageHash = sha256.New()
		case "sha512":
			downloadedImageHash = sha512.New()
		default:
			return fmt.Errorf("unknown hash format: %s", fragmenthash[0])
		}
		expectedHash = fragmenthash[1]
	} else if strings.HasPrefix(imageURL, "https://") {
		logrus.Warnf("downloading image without verifying the hash from %s", imageURL)
	}

	if downloadedImageHash == nil {
		downloadedImageHash = sha256.New()
	}

	if parsedImageURL.Scheme == "https" || parsedImageURL.Scheme == "http" {
		// check for cached image first
		cacheHash := expectedHash
		if cacheHash == "" {
			cacheHash = fmt.Sprintf("%x", sha256.Sum256([]byte(imageURL)))
		}
		cachedImage := filepath.Join(config.C.ImageCache, cacheHash)

		logrus.Debug("checking for cache file ", cachedImage, " before downloading from ", imageURL)

		localfile, err := os.Open(cachedImage)
		if err != nil {
			if !os.IsNotExist(err) {
				logrus.Error("erro reading cache file ", cachedImage, ": ", err)
			}
			logrus.Debugf("no cached file, downloading %s", imageURL)

			ctx, cancel := context.WithTimeout(context.Background(), time.Hour)
			defer cancel()
			resp, err := httpx.GetHTTPClient(ctx).Get(imageURL)
			if err != nil {
				return err
			}
			defer resp.Body.Close()

			if resp.StatusCode != http.StatusOK {
				logrus.Warn("error fetching image: ", resp.Status)
				return fmt.Errorf("error fetching base image: %s", resp.Status)
			}

			reader = resp.Body
			contentLength = resp.ContentLength

			if err := os.MkdirAll(config.C.ImageCache, 0755); err != nil {
				logrus.Error("error ensuring image cache directory ", config.C.ImageCache, ": ", err)
			}

			// localfile, err := os.Create(cachedImage)
			// if err != nil {
			// 	log.Error("error creating cache file: ", err)
			// } else {
			// 	log.Info("caching to ", cachedImage)
			// 	defer localfile.Close()

			// 	reader = io.TeeReader(reader, localfile)
			// }
		} else {
			defer localfile.Close()

			stat, err := localfile.Stat()
			if err != nil {
				logrus.Warn("error checking metadata on cached image ", cachedImage)
				return err
			}

			reader = localfile
			contentLength = stat.Size()
		}

	} else if parsedImageURL.Scheme == "" {
		logrus.Debugf("reading local file %s for root volume", parsedImageURL.Path)

		localfile, err := os.Open(parsedImageURL.Path)
		if err != nil {
			return err
		}
		defer localfile.Close()

		stat, err := localfile.Stat()
		if err != nil {
			return err
		}

		reader = localfile
		contentLength = stat.Size()
	} else {
		return fmt.Errorf("unsupported image URL scheme %s", parsedImageURL.Scheme)
	}

	if downloadedImageHash != nil {
		reader = io.TeeReader(reader, downloadedImageHash)
	}

	// check for known compression suffixes and decompress
	// can also be enabled by adding to the URL fragment:
	// https://example.whatever/img.qcow2#compression=xz
	fragmentCompression := fragmentQuery.Get("compression")
	if strings.HasSuffix(parsedImageURL.Path, ".xz") || fragmentCompression == "xz" {
		logrus.Debug("image is xz compressed, decompressing")
		reader, err = xz.NewReader(reader, 0)
		if err != nil {
			return err
		}
	} else if strings.HasSuffix(parsedImageURL.Path, ".gz") || fragmentCompression == "gzip" {
		logrus.Debug("image is gz compressed, decompressing")
		reader, err = gzip.NewReader(reader)
		if err != nil {
			return err
		}
	} else if strings.HasSuffix(parsedImageURL.Path, ".bz2") || fragmentCompression == "bzip2" {
		logrus.Debug("image is bzip2 compressed, decompressing")
		reader = bzip2.NewReader(reader)
	}

	stream, err := conn.NewStream(0)
	if err != nil {
		return err
	}
	defer libvirtx.Free(stream)

	err = volume.Upload(stream, 0, 0, 0)
	if err != nil {
		return err
	}

	logrus.Debug("uploading to volume")
	totalRead := 0
	nextLog := time.Now()
	err = stream.SendAll(func(s *libvirt.Stream, i int) ([]byte, error) {
		out := []byte{}

		for len(out) < i {
			buf := make([]byte, i-len(out))
			// fill the buffer of i bytes from the reader
			read, err := reader.Read(buf)
			if read > 0 {
				out = append(out, buf[:read]...)
				totalRead += read
			}
			if (contentLength > 0 && time.Until(nextLog) <= 0) || (int64(totalRead) == contentLength && read > 0) {
				logrus.Debug("transfer progress: ", totalRead, "/", contentLength, " (", int((float64(totalRead)/float64(contentLength))*100), "%) ", time.Since(start).Round(time.Millisecond))
				nextLog = time.Now().Add(time.Second * 5)
			}
			if err == io.EOF {
				if read > 0 {
					// partial read before hitting EOF, return buffer
					return out, nil
				}
				// Go's io interface raise an io.EOF error to indicate the end of the stream,
				// but libvirt indicates EOF with a zero-length response
				return make([]byte, 0), nil
			}
			if err != nil {
				logrus.Println("Error while reading buffer", err.Error())
				return out, err
			}
		}
		return out, nil
	})
	if err != nil {
		return err
	}

	hashstr := fmt.Sprintf("%x", downloadedImageHash.Sum(nil))

	if expectedHash != "" {
		if hashstr != expectedHash {
			return fmt.Errorf("got bad hash when downloading %s: got %s expected %s", imageURL, hashstr, expectedHash)
		}
		logrus.Debug("hash of downloaded image matched expected")
	} else {
		logrus.Infof("downloaded %s (sha256: %s)", imageURL, hashstr)
	}

	// qcow2 quirk: because we download a qcow2 file instead of the raw file, the server-provided metadata about the virtual
	// volume is left in tact, which sets the virtual disk size to the size the server wants it to be, ignoring our requested
	// size. To work around this, we resize the virtual disk size before first boot.
	pool, err := pools.GetPool(conn)
	if err != nil {
		return err
	}
	defer libvirtx.Free(pool)

	volname, err := volume.GetName()
	if err != nil {
		return err
	}

	if pool.Type() == pools.StoragePoolTypeDir {
		if err := pool.ResizeVolume(volname, uint64(sizeGB)); err != nil {
			logrus.Error("error resizing volume ", volname, " after download: ", err)
			return err
		}
	}

	return nil
}

func getHashes(image string) (map[string]string, error) {
	url := fmt.Sprintf("%s/%s/SHA256SUMS", config.C.ImageHost, image)
	logrus.Debugf("getting URL %s", url)
	resp, err := http.Get(url)
	if err != nil {
		return map[string]string{}, err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return map[string]string{}, fmt.Errorf("error fetching base image: %s", resp.Status)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return map[string]string{}, err
	}

	hashes := make(map[string]string)

	for i, line := range strings.Split(string(body), "\n") {
		if line == "" {
			continue
		}
		lineparts := strings.Split(line, "  ")
		if len(lineparts) != 2 {
			logrus.Printf("Unable to parse line %d", i)
			continue
		}
		hashes[lineparts[1]] = lineparts[0]
	}
	return hashes, nil
}
