#!/bin/sh
set -x

export LIBVIRT_DEFAULT_URI=qemu:///system

virsh list --all | grep rhyzome | awk '{ print $2 }' | while read d; do
    virsh destroy "$d"
    virsh undefine "$d"
done

virsh vol-list --pool default | grep rhyzome | awk '{ print $1 }' | while read vol; do
    virsh vol-delete --pool default "$vol"
done

virsh net-destroy entanglement-bootstrap-network

rm -rf pki join-token.txt

true # always exit 0
