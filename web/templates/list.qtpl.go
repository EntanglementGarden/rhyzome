// Code generated by qtc from "list.qtpl". DO NOT EDIT.
// See https://github.com/valyala/quicktemplate for details.

// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only
//

//line web/templates/list.qtpl:5
package templates

//line web/templates/list.qtpl:5
import (
	"entanglement.garden/rhyzome/web/openapi"
)

//line web/templates/list.qtpl:10
import (
	qtio422016 "io"

	qt422016 "github.com/valyala/quicktemplate"
)

//line web/templates/list.qtpl:10
var (
	_ = qtio422016.Copy
	_ = qt422016.AcquireByteBuffer
)

//line web/templates/list.qtpl:11
type InstanceList struct {
	Instances []openapi.Instance
}

//line web/templates/list.qtpl:16
func (p *InstanceList) StreamTitle(qw422016 *qt422016.Writer) {
//line web/templates/list.qtpl:16
	qw422016.N().S(`Instances`)
//line web/templates/list.qtpl:16
}

//line web/templates/list.qtpl:16
func (p *InstanceList) WriteTitle(qq422016 qtio422016.Writer) {
//line web/templates/list.qtpl:16
	qw422016 := qt422016.AcquireWriter(qq422016)
//line web/templates/list.qtpl:16
	p.StreamTitle(qw422016)
//line web/templates/list.qtpl:16
	qt422016.ReleaseWriter(qw422016)
//line web/templates/list.qtpl:16
}

//line web/templates/list.qtpl:16
func (p *InstanceList) Title() string {
//line web/templates/list.qtpl:16
	qb422016 := qt422016.AcquireByteBuffer()
//line web/templates/list.qtpl:16
	p.WriteTitle(qb422016)
//line web/templates/list.qtpl:16
	qs422016 := string(qb422016.B)
//line web/templates/list.qtpl:16
	qt422016.ReleaseByteBuffer(qb422016)
//line web/templates/list.qtpl:16
	return qs422016
//line web/templates/list.qtpl:16
}

//line web/templates/list.qtpl:18
func (p *InstanceList) StreamBody(qw422016 *qt422016.Writer) {
//line web/templates/list.qtpl:18
	qw422016.N().S(`
<form method="post">
<!--
<button type="submit" formaction="/entanglement.garden/rhyzome/instance/delete" class="button">Delete</button>
<button type="submit" formaction="/entanglement.garden/rhyzome/instance/edit" class="button">Edit</button>
-->
<table>
    `)
//line web/templates/list.qtpl:25
	for _, instance := range p.Instances {
//line web/templates/list.qtpl:25
		qw422016.N().S(`
        <tr>
            <td><input type="checkbox" name="instances" value="`)
//line web/templates/list.qtpl:27
		qw422016.E().S(instance.ResourceId)
//line web/templates/list.qtpl:27
		qw422016.N().S(`" /></td>
            <td><a href="/entanglement.garden/rhyzome/instance/`)
//line web/templates/list.qtpl:28
		qw422016.E().S(instance.ResourceId)
//line web/templates/list.qtpl:28
		qw422016.N().S(`">`)
//line web/templates/list.qtpl:28
		qw422016.E().S(instance.ResourceId)
//line web/templates/list.qtpl:28
		qw422016.N().S(`</a></td>
            <td>`)
//line web/templates/list.qtpl:29
		qw422016.E().S(string(instance.State))
//line web/templates/list.qtpl:29
		qw422016.N().S(`</td>
        </tr>
    `)
//line web/templates/list.qtpl:31
	}
//line web/templates/list.qtpl:31
	qw422016.N().S(`
</table>
</form>
`)
//line web/templates/list.qtpl:34
}

//line web/templates/list.qtpl:34
func (p *InstanceList) WriteBody(qq422016 qtio422016.Writer) {
//line web/templates/list.qtpl:34
	qw422016 := qt422016.AcquireWriter(qq422016)
//line web/templates/list.qtpl:34
	p.StreamBody(qw422016)
//line web/templates/list.qtpl:34
	qt422016.ReleaseWriter(qw422016)
//line web/templates/list.qtpl:34
}

//line web/templates/list.qtpl:34
func (p *InstanceList) Body() string {
//line web/templates/list.qtpl:34
	qb422016 := qt422016.AcquireByteBuffer()
//line web/templates/list.qtpl:34
	p.WriteBody(qb422016)
//line web/templates/list.qtpl:34
	qs422016 := string(qb422016.B)
//line web/templates/list.qtpl:34
	qt422016.ReleaseByteBuffer(qb422016)
//line web/templates/list.qtpl:34
	return qs422016
//line web/templates/list.qtpl:34
}
