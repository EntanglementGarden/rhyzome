// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/web/openapi"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
	webuitemplates "entanglement.garden/webui-plugin/templates"
)

func stopInstance(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	log := logging.GetLog(ctx)

	form, err := url.ParseQuery(request.Body)
	if err != nil {
		log.Warn("error parsing request form: ", err)
		return nil, err
	}

	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	rhyzome, err := openapi.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	id := form.Get("instance")
	resp, err := rhyzome.StopInstanceWithResponse(ctx, id)
	if err != nil {
		log.Error("error fetching instance: ", err)
		return nil, err
	}

	if resp.StatusCode() != http.StatusNoContent {
		log.Error("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body))
		return plugin.WritePage(ctx, request, &webuitemplates.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body)),
		})
	}

	return plugin.Redirect(fmt.Sprintf("/instance/%s", id))
}

func startInstance(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	log := logging.GetLog(ctx)

	form, err := url.ParseQuery(request.Body)
	if err != nil {
		log.Warn("error parsing request form: ", err)
		return nil, err
	}

	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	rhyzome, err := openapi.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	id := form.Get("instance")
	resp, err := rhyzome.StartInstanceWithResponse(ctx, id)
	if err != nil {
		log.Error("error fetching instance: ", err)
		return nil, err
	}

	if resp.StatusCode() != http.StatusNoContent {
		log.Error("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body))
		return plugin.WritePage(ctx, request, &webuitemplates.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body)),
		})

	}

	return plugin.Redirect(fmt.Sprintf("/instance/%s", id))
}
