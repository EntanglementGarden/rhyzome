// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"

	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/web/openapi"
	"entanglement.garden/rhyzome/web/templates"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
	webuitemplates "entanglement.garden/webui-plugin/templates"
)

func listInstancesPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	log := logging.GetLog(ctx)

	eg, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	rhyzome, err := openapi.NewClient(eg)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	instances, err := rhyzome.ListAllInstancesWithResponse(ctx)
	if err != nil {
		log.Error("error fetching instance list: ", err)
		return nil, err
	}

	if instances.JSON200 == nil {
		log.Error("unexpected ", instances.HTTPResponse.Status, " from server: ", string(instances.Body))
		return plugin.WritePage(ctx, request, &webuitemplates.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", instances.HTTPResponse.Status, " from server: ", string(instances.Body)),
		})
	}

	return plugin.WritePage(ctx, request, &templates.InstanceList{Instances: *instances.JSON200})
}
