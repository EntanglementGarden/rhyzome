// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"
	"net/http"

	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/web/openapi"
	"entanglement.garden/rhyzome/web/templates"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
	webuitemplates "entanglement.garden/webui-plugin/templates"
)

func deleteInstancePage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	return plugin.WritePage(ctx, request, &templates.InstanceDelete{ResourceID: request.PathParams["instance"]})
}

func deleteInstance(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	log := logging.GetLog(ctx)

	eg, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	rhyzome, err := openapi.NewClient(eg)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	resp, err := rhyzome.DeleteInstanceWithResponse(ctx, request.PathParams["instance"])
	if err != nil {
		log.Error("error building api client: ", err)
		return nil, err
	}

	if resp.StatusCode() != http.StatusNoContent {
		log.Error("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body))
		return plugin.WritePage(ctx, request, &webuitemplates.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body)),
		})
	}

	return plugin.Redirect("/")
}
