// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"

	"entanglement.garden/api-client/egnetworking"
	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/web/openapi"
	"entanglement.garden/rhyzome/web/templates"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
	webuitemplates "entanglement.garden/webui-plugin/templates"
)

func instancePage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	log := logging.GetLog(ctx)

	eg, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection")
		return nil, err
	}

	rhyzome, err := openapi.NewClient(eg)
	if err != nil {
		log.Warn("error preparing networking client connection")
		return nil, err
	}

	instance, err := rhyzome.GetInstanceWithResponse(ctx, request.PathParams["instance"])
	if err != nil {
		log.Error("error fetching instance")
		return nil, err
	}

	if instance.JSON200 == nil {
		log.Error("unexpected ", instance.HTTPResponse.Status, " from server: ", string(instance.Body))
		return plugin.WritePage(ctx, request, &webuitemplates.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", instance.HTTPResponse.Status, " from server: ", string(instance.Body)),
		})
	}

	networking, err := egnetworking.NewClient(eg)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	portForwards := map[string][]egnetworking.PortForward{}
	for _, nic := range instance.JSON200.Interfaces {
		resp, err := networking.GetPortForwardsWithResponse(ctx, &egnetworking.GetPortForwardsParams{
			Host: &nic.Id,
		})
		if err != nil {
			log.Error("error fetching port forwards for instance: ", err)
			return nil, err
		}

		if resp.JSON200 == nil {
			log.Error("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body))
			return plugin.WritePage(ctx, request, &webuitemplates.Error{
				ErrTitle:    "invalid response from server",
				Description: fmt.Sprint("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body)),
			})
		}

		portForwards[nic.Id] = *resp.JSON200
	}

	return plugin.WritePage(ctx, request, &templates.GetInstance{
		Instance:     instance.JSON200,
		PortForwards: portForwards,
	})
}
