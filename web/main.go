// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"net/http"

	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
)

var p = plugin.Plugin{
	Name:      "Rhyzome",
	Extension: "entanglement.garden/rhyzome",
	Endpoints: map[string]map[string]plugin.Endpoint{
		"/": {
			http.MethodGet: listInstancesPage,
		},
		"/create": {
			http.MethodGet:  createInstancePage,
			http.MethodPost: createInstance,
		},
		"/instance/:instance": {
			http.MethodGet: instancePage,
		},
		"/instance/:instance/delete": {
			http.MethodGet:  deleteInstancePage,
			http.MethodPost: deleteInstance,
		},
		"/instance/:instance/start": {
			http.MethodPost: startInstance,
		},
		"/instance/:instance/stop": {
			http.MethodPost: stopInstance,
		},
	},
	Menu: &plugin_protos.MenuItem{
		Name: "VMs",
		Path: "/",
		Subitems: []*plugin_protos.MenuItem{
			{
				Name: "List",
				Path: "/",
			},
			{
				Name: "Create",
				Path: "/create",
			},
		},
	},
}

func main() {
	p.ListenAndServe()
}
