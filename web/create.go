// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"
	"net/url"
	"strconv"

	"entanglement.garden/api-client/egnetworking"
	"entanglement.garden/api-client/egrhyzome"
	"entanglement.garden/common/logging"
	"entanglement.garden/rhyzome/web/templates"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
	webuitemplates "entanglement.garden/webui-plugin/templates"
)

func createInstance(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	log := logging.GetLog(ctx)

	form, err := url.ParseQuery(request.Body)
	if err != nil {
		log.Debug("error parsing request form: ", err)
		return nil, err
	}

	memory, err := strconv.ParseInt(form.Get("memory"), 10, 32)
	if err != nil {
		log.Debug("invalid memory (not a float): ", err)
		return nil, err
	}

	cores, err := strconv.ParseInt(form.Get("cores"), 10, 32)
	if err != nil {
		log.Debug("invalid cores (not an int): ", err)
		return nil, err
	}

	disk, err := strconv.ParseInt(form.Get("disk-size"), 10, 32)
	if err != nil {
		log.Debug("invalid disk-size (not an int): ", err)
		return nil, err
	}

	userData := form.Get("user-data")

	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	networking, err := egnetworking.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	network := form.Get("network")
	if network == "" {
		createdNetwork, err := networking.CreateNetworkWithResponse(ctx, egnetworking.CreateNetworkBody{})
		if err != nil {
			log.Warn("error creating network:", err)
			return nil, err
		}

		if createdNetwork.JSON201 == nil {
			log.Error("unexpected ", createdNetwork.HTTPResponse.Status, " from server: ", string(createdNetwork.Body))
			return plugin.WritePage(ctx, request, &webuitemplates.Error{
				ErrTitle:    "invalid response from server",
				Description: fmt.Sprint("unexpected ", createdNetwork.HTTPResponse.Status, " from server: ", string(createdNetwork.Body)),
			})
		}

		network = createdNetwork.JSON201.Id
	}

	rhyzome, err := egrhyzome.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	instance, err := rhyzome.CreateInstanceWithResponse(ctx, egrhyzome.CreateInstanceRequestBody{
		BaseImage: form.Get("base-image"),
		Memory:    int(memory),
		Cpu:       int(cores),
		DiskSize:  int(disk),
		UserData:  &userData,
		Networks:  []string{network},
	})
	if err != nil {
		return nil, err
	}

	if instance.JSON201 == nil {
		log.Error("unexpected ", instance.HTTPResponse.Status, " from server: ", string(instance.Body))
		return plugin.WritePage(ctx, request, &webuitemplates.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", instance.HTTPResponse.Status, " from server: ", string(instance.Body)),
		})
	}

	return plugin.Redirect(fmt.Sprintf("/instances/%s", instance.JSON201.ResourceId))
}

func createInstancePage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	log := logging.GetLog(ctx)

	eg, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection")
		return nil, err
	}

	networking, err := egnetworking.NewClient(eg)
	if err != nil {
		log.Warn("error preparing networking client connection")
		return nil, err
	}

	networks, err := networking.ListAllNetworksWithResponse(ctx)
	if err != nil {
		return nil, err
	}

	if networks == nil {
		return plugin.WritePage(ctx, request, &webuitemplates.Error{ErrTitle: "invalid response from backend"})
	}

	if networks.JSON200 == nil {
		log.Error("unexpected ", networks.HTTPResponse.Status, " from backend server: ", string(networks.Body))
		return plugin.WritePage(ctx, request, &webuitemplates.Error{
			ErrTitle:    "invalid response from backend",
			Description: fmt.Sprint("unexpected ", networks.HTTPResponse.Status, " from backend server: ", string(networks.Body)),
		})
	}

	return plugin.WritePage(ctx, request, &templates.InstanceCreate{Networks: networks.JSON200})
}
