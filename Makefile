all: rhyzome-agent rhyzome-server

rhyzome-agent: protos/rhyzome.pb.go client/go.sum $(shell find client -name '*.go' -print)
	cd client && go build -o ../rhyzome-agent -ldflags "-X entanglement.garden/common/config.Version=$(shell git describe --always --tags)" ./cmd/rhyzome-agent

rhyzome-server: protos/rhyzome.pb.go server/openapi/openapi.go server/db/db.go server/go.sum server/httpserver/web.ewp $(shell find server -name '*.go' -print)
	cd server && go build -o ../rhyzome-server -ldflags "-X entanglement.garden/common/config.Version=$(shell git describe --always --tags)" ./cmd/rhyzome-server

server/openapi/openapi.go: server/openapi/spec.yaml
	eg dev codegen openapi-server server/openapi/spec.yaml server/openapi/openapi.go

server/db/db.go: server/db/sqlite/migrations/*.sql server/db/sqlite/queries/*.sql server/sqlc.yaml
	cd server && sqlc generate

protos/rhyzome.pb.go: protos/rhyzome.proto
	protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative protos/*.proto

install-libvirt: rhyzome rhyzome-hook-qemu
	cp rhyzome /usr/bin/rhyzome

	mkdir -p /var/lib/libvirt/hooks/rhyzome.d
	cp rhyzome-hook-qemu /etc/libvirt/hooks/rhyzome.d/qemu

entanglement-garden-rhyzome.json:
	cp entanglement-garden-rhyzome.localdev.json entanglement-garden-rhyzome.json

server/go.sum: server/go.mod
	cd server && go mod tidy

server/httpserver/web.ewp: web/templates/create.qtpl.go web/templates/get.qtpl.go web/templates/list.qtpl.go $(shell find web/ -name '*.go' -print)
	cd web && go build -trimpath -ldflags "-X entanglement.garden/common/config.Version=$(shell git describe --always --tags)" -o ../server/httpserver/web.ewp

web/templates/*.qtpl.go: $(shell find web/templates -name '*.qtpl' -print)
	qtc -dir web/templates

clean:
	-rm rhyzome-agent rhyzome-server server/httpserver/web.ewp
