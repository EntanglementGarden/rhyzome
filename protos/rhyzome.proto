// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

syntax = "proto3";

package protos;
option go_package = "entanglement.garden/rhyzome/protos";

service Rhyzome {
    rpc EventStream(stream EventUpdate) returns (stream Event) {}
    rpc InstanceStateChange(Instance) returns (Empty) {}
    rpc VolumeStateChange(Volume) returns (Empty) {}
    rpc Import(Event) returns (Empty) {}
}

// EventUpdate instances are streamed from a rhyzome node to the central control server
message EventUpdate {
    enum Type {
        ASYNC = 0; // ASYNC type for async updates not triggered by a server-side event. Please help me think of a better name. Also currently unused
        STARTED = 1;
        UPDATE = 2;
        SUCCESS = 3;
        FAILED = 4;
    }
    // string event_id = 1;
    Type type = 2;
    string message = 3;
}

// Event instances are streamed from the central control server to a rhyzome node
message Event {
    Instance instance = 2;
    Volume volume = 3;
    bool bootstrap_complete = 4; // when set to true, indicates the bootstrap process is complete and bootstrap-related things should be cleaned up
}


message Instance {
    enum State {
        PENDING = 0;
        CREATING = 1;
        STARTING = 2;
        RUNNING = 3;
        STOPPING = 4;
        STOPPED = 5;
        DELETING = 6;
        DELETED = 7;
        ERROR = 8;
        REBOOTING = 9;
    }
    enum SpecialCase {
        ROUTER = 0;
        BOOTSTRAP_NETWORK = 1;
        SYSTEM_NETWORK = 2;
    }
    string id = 1;
    State state = 2;
    string name = 3;
    string root_volume = 4;
    int32 memory = 5;
    int32 cores = 6;
    repeated string volumes = 7;
    repeated NetworkInterface network_interfaces = 8;
    repeated SpecialCase special_cases = 9;
    string biosdata = 10;
}

message NetworkInterface {
    string id = 1;
    string mac = 2;
    string ip = 3;
    repeated int32 vlan = 4;
    bool tagged = 5;
    bool wan = 6;
    bool bootstrap = 7;
}

message Volume {
    enum State {
        PENDING = 0;
        CREATING = 1;
        UNATTACHED = 2;
        ATTACHED = 3;
        DELETED = 4;
        ERROR = 5;
    }
    string id = 1;
    int32 size = 2;
    string source_image = 3;
    string source_image_qcow = 4;
    State state = 5;
}

message Empty {}
