# Rhyzome

This is the Entanglement Garden component for provisioning VMs using libvirt.

there are two binaries: rhyzome-server, which runs in the cloud,
and rhyzome, which runs on the Linux hypervisor next to libvirt.

It facilates the creation of virtual machines and unikernels.


## Build

requires

* `Go >=1.19`
* `libvirt-headers >=5.0.0`

Build with `make`

## Architecture
A REST server (`rhyzome-server`) accepts JWT-authenticated requests, stores the expected state in a sqlite database. It also creates a gRPC listener.
On or more nodes, the client (`rhyzome-agent`) runs and connects to the gRPC listener, registering the node with the server. The client then listens for events
from the server. The server initially sends all expected state (ie. all volumes and VMs that are supposed to be on the host) to the client, then sends new updates
as they come in.

# Installing

*note that a typical installation is performed by the [Entanglement Installer](https://codeberg.org/EntanglementGarden/installer), which will install and configure rhyzome with the rest of the system*

`rhyzome-agent` needs a libvirt hook installed. This requires several things:

* the `rhyzome-agent` binary should be available in the $PATH. I put it in `/usr/local/bin/rhyzome-agent`
* the actual hook, at `/etc/libvirt/hooks/qemu.d/rhyzome` must be a shell script (per libvirt docs). Here's mine:
```bash
#!/bin/bash
rhyzome-agent hook $@
```
* apparmor should allow libvirt to update it. I added this line to `/etc/apparmor.d/local/usr.sbin.libvirtd`:
```
/usr/local/bin/rhyzome-agent ix,
```
* `/var/run/rhyzome-agent` must exist and be accessible by the user `rhyzome-agent` is running as

## Dev setup

The server component can be tested locally with containers just like most of the other parts of rhyzome.

The client component must run on a system with libvirt and openvswitch, both of which I installed on my dev machine. To develop on the client, bring
up the server with the rest of the services like normal (use `start-services.sh` in the local dev repo), copy `rhyzome.localdev.json` to
`rhyzome.json` and run the client with `go run ./cmd/rhyzome-agent`.
