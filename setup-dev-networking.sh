#!/bin/bash -ex

modprobe tap

ovs-vsctl add-br br0
ovs-vsctl add-port br0 rhyzome0 tag=10 -- set interface rhyzome0 type=internal ofport=1

net=$(mktemp)

echo "<network>
  <name>rhyzome-wan</name>
  <forward mode='nat'></forward>
  <ip address='192.168.200.1'>
    <dhcp>
      <range start='192.168.200.128' end='192.168.200.254'/>
    </dhcp>
  </ip>
</network>" > "${net}"

virsh net-create "${net}"
