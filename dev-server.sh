#!/bin/bash
set -exuo pipefail
make rhyzome-server entanglement-garden-rhyzome.json
podman build -t rhyzome -f - . <<EOF
FROM gcr.io/distroless/base-debian11:nonroot
ADD rhyzome-server /rhyzome-server
CMD ["/rhyzome-server"]
EOF
podman run --rm --network entanglement-devtools --env-file "$(pwd)/../devtools/entanglement-common.env" --name rhyzome -v $(pwd)/../devtools/entanglement.garden:/etc/entanglement.garden -v $(pwd)/../devtools/state/step:/etc/step:ro -v $(pwd)/../devtools/state/rhyzome:/var/entanglement.garden/rhyzome -p 9090:9090 rhyzome
