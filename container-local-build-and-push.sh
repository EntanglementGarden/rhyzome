#!/bin/bash
set -exuo pipefail
branch=$(git rev-parse --abbrev-ref HEAD)
image="codeberg.org/entanglementgarden/rhyzome:${branch}"
make clean
make rhyzome-server
podman build -t "${image}" -f - . <<EOF
FROM debian:stable
RUN apt-get update && apt-get install delve
ADD rhyzome-server /rhyzome-server
CMD ["/rhyzome-server"]
EOF

podman push "${image}"
